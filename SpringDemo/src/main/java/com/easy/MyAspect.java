package com.easy;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @ClassName MyAspect
 * @Description TODO
 * @Author zheng
 * @Date 2023/4/27 17:46
 * @Version 1.0
 **/
@Aspect
@Component
public class MyAspect {
    // 为Student这个类的所有方法，配置这个前置通知
    //@Before("*execution(com.easy.service.UserServiceImpl.*(*)")
    @Before("execution(* com.easy.domain.*.*(..))")
    public void before() {
        System.out.println("before student");
    }
}
