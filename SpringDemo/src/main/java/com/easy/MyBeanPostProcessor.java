package com.easy;

import com.easy.domain.ClassA;
import com.easy.proxy.JdkProxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.SmartFactoryBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.SmartInstantiationAwareBeanPostProcessor;
import org.springframework.lang.Nullable;

/**
 * @ClassName MyBeanPostProcessor
 * @Description TODO
 * @Author zheng
 * @Date 2023/4/25 16:36
 * @Version 1.0
 **/
public class MyBeanPostProcessor implements SmartInstantiationAwareBeanPostProcessor {
    public Object getEarlyBeanReference(Object bean, String beanName) throws BeansException {
        if (bean instanceof ClassA) {
            return new JdkProxy(bean).getProxy();
        }
        return bean;
    }
}
