package com.easy.event;

import org.springframework.context.ApplicationEvent;

/**
 * @ClassName MyEvent
 * @Description TODO
 * @Author zheng
 * @Date 2023/4/12 17:15
 * @Version 1.0
 **/
public class MyEvent extends ApplicationEvent {
    private String msg;

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public MyEvent(Object source) {
        super(source);
    }

    public MyEvent(Object source, String msg) {
        super(source);
        this.msg = msg;
    }

    public void sendMessage() {
        System.out.println("msg is :" + msg);
    }
}
