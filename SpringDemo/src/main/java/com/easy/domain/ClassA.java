package com.easy.domain;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName ClassA
 * @Description TODO
 * @Author zheng
 * @Date 2023/4/25 10:20
 * @Version 1.0
 **/
public class ClassA {
    @Autowired
    private ClassB classB;

    public ClassA() {
        System.out.println("ClassA initialized");
    }

    public void test() {
        System.out.println("classA test method");
    }
}
