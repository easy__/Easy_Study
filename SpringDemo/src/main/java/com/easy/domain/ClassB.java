package com.easy.domain;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName ClassB
 * @Description TODO
 * @Author zheng
 * @Date 2023/4/25 10:20
 * @Version 1.0
 **/
public class ClassB {
    @Autowired
    private ClassA classA;

    public ClassB() {
        System.out.println("ClassB initialized");
    }
}
