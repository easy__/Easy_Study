package com.easy.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName Student
 * @Description TODO
 * @Author zheng
 * @Date 2023/5/4 19:25
 * @Version 1.0
 **/
@Component
public class Student {
    @Autowired
    private User user;
    public Student() {
        System.out.println("Student init");
    }
}
