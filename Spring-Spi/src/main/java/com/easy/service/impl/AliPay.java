package com.easy.service.impl;

import com.easy.service.PayService;

/**
 * @ClassName AliPay
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/29 10:35
 * @Version 1.0
 **/
public class AliPay implements PayService {
    @Override
    public String getPayMethod() {
        return "AliPay";
    }
}
