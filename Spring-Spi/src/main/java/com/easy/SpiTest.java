package com.easy;

import com.easy.config.MyConfig;
import com.easy.service.PayService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @ClassName SpiTest
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/29 11:01
 * @Version 1.0
 **/
public class SpiTest {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(MyConfig.class);
        PayService payService = applicationContext.getBean(PayService.class);
        System.out.println(payService.getPayMethod());
    }

}
