package com.easy.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName MyConfig
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/29 10:37
 * @Version 1.0
 **/
@ComponentScan("com.easy")
@Configuration
public class MyConfig {
}
