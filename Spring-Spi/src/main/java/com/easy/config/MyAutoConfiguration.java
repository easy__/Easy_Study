package com.easy.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @ClassName MyAutoConfiguration
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/29 10:41
 * @Version 1.0
 **/
@Configuration
@Import(MyAutoConfigurationRegister.class)
public class MyAutoConfiguration {
}
