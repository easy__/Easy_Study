package com.jxy.pay.strategy;

import com.jxy.pay.domain.PayPara;
import com.jxy.pay.domain.RefundPara;

/**
 * @author zheng
 */
public interface PayStrategy {
    /**
     * @Description 支付方法
     * @Date 2022/10/11 10:38
     * @Param []
     **/
    public String pay(PayPara payPara);

    /**
     * @Description 支付方法
     * @Date 2022/10/11 10:38
     * @Param []
     **/
    public String refund(RefundPara refundPara);
}
