package com.jxy.pay.strategy;

import com.jxy.pay.domain.PayPara;
import com.jxy.pay.domain.RefundPara;

/**
 * @ClassName PayContext
 * @Description 支付上下文
 * @Author zheng
 * @Date 2022/10/11 14:32
 * @Version 1.0
 **/
public class PayContext {
    private final PayStrategy payStrategy;

    public PayContext(PayStrategy payStrategy) {
        this.payStrategy = payStrategy;
    }

    public String pay(PayPara payPara) {
        return payStrategy.pay(payPara);
    }

    public String refund(RefundPara refundPara) {
        return payStrategy.refund(refundPara);
    }
}
