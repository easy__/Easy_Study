package com.jxy.pay.strategy;

import com.jxy.pay.domain.PayPara;
import com.jxy.pay.domain.RefundPara;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName WechatPayStrategy
 * @Description 微信支付策略
 * @Author zheng
 * @Date 2022/10/11 10:53
 * @Version 1.0
 **/
@Slf4j
public class WechatPayStrategy implements PayStrategy {
    @Override
    public String pay(PayPara payPara) {
        log.info("订单[{}]支付成功，支付金额为{}", payPara.getOrderId(), payPara.getTotal());
        return String.format("订单%s微信支付成功，支付金额为%s", payPara.getOrderId(), payPara.getTotal());
    }

    @Override
    public String refund(RefundPara refundPara) {
        log.info("订单[{}]退款成功", refundPara.getOrderId());
        return String.format("订单%s微信退款成功", refundPara.getOrderId());
    }
}
