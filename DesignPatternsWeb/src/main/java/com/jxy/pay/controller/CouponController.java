package com.jxy.pay.controller;

import com.jxy.pay.domain.AjaxResult;
import com.jxy.pay.domain.UserInfo;
import com.jxy.pay.service.ICouponService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName CouponController
 * @Description 优惠券（责任链模式，进行是否有优惠券领取资格校验）
 * @Author zheng
 * @Date 2022/10/20 14:36
 * @Version 1.0
 **/
@RestController
@RequestMapping("/coupon")
public class CouponController {

    private final ICouponService couponService;

    public CouponController(ICouponService couponService) {
        this.couponService = couponService;
    }

    /**
     * @return void
     * @Description 获取优惠券
     * @Date 2022/10/20 14:43
     * @Param [userInfo]
     **/
    @PostMapping
    public AjaxResult getCoupon(@RequestBody UserInfo userInfo) {
        try {
            couponService.getCoupon(userInfo);
            return AjaxResult.success("优惠券获取成功");
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }
}
