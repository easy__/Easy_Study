package com.jxy.pay.controller;

import com.jxy.pay.domain.AjaxResult;
import com.jxy.pay.domain.PayPara;
import com.jxy.pay.domain.RefundPara;
import com.jxy.pay.service.IPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @ClassName PayController
 * @Description 支付
 * @Author zheng
 * @Date 2022/10/11 14:39
 * @Version 1.0
 **/
@RestController
@RequestMapping("/pay")
public class PayController {
    private final IPayService payService;

    public PayController(IPayService payService) {
        this.payService = payService;
    }

    @PostMapping("/pay")
    public AjaxResult pay(@RequestBody @Valid PayPara payPara) {
        return AjaxResult.success(payService.pay(payPara));
    }
    @PostMapping("/refund")
    public AjaxResult refund(@RequestBody @Valid RefundPara refundPara) {
        return AjaxResult.success(payService.refund(refundPara));
    }
}
