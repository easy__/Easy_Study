package com.jxy.pay.controller;

import com.jxy.pay.composite.DeptItem;
import com.jxy.pay.service.IDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName DeptController
 * @Description TODO
 * @Author zheng
 * @Date 2022/10/20 20:38
 * @Version 1.0
 **/
@RestController
@RequestMapping("/dept")
public class DeptController {
    @Autowired
    private IDeptService deptService;

    @GetMapping("/list")
    public DeptItem list() {
        return deptService.list();
    }

    @PostMapping("/add")
    public void add(@RequestBody DeptItem item) {
        deptService.add(item);
    }

    @DeleteMapping("/{id}}")
    public void del(@PathVariable int id) {

    }

    @GetMapping("/print")
    public void print() {
        deptService.print();
    }
}
