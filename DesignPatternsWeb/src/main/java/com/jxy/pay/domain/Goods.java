package com.jxy.pay.domain;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName Goods
 * @Description TODO
 * @Author zheng
 * @Date 2022/10/13 19:51
 * @Version 1.0
 **/
@Data
public class Goods {
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品描述
     */
    private String goodsDescription;
    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;
    /**
     * 可投放城市
     */
    private String city;
    /**
     * 新用户才投放
     */
    private boolean newUser;
}
