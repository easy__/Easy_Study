package com.jxy.pay.domain;

import lombok.Data;

/**
 * @ClassName UserInfo
 * @Description 用户信息
 * @Author zheng
 * @Date 2022/10/13 20:06
 * @Version 1.0
 **/
@Data
public class UserInfo {
    /**
     * 所在城市
     */
    private String city;
    /**
     * 是否为新用户
     */
    private boolean newUser;
    /**
     * 优惠券编号
     */
    private String couponNumber;
    /**
     * 用户编号
     */
    private String userId;
}
