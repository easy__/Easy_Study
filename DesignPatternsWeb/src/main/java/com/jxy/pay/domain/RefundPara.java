package com.jxy.pay.domain;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName RefundPara
 * @Description TODO
 * @Author zheng
 * @Date 2022/10/13 16:26
 * @Version 1.0
 **/
@Data
public class RefundPara {
    /**
     * 订单id
     */
    @NotBlank(message = "订单id[orderId]不能为空")
    private String orderId;

    /**
     * 支付方式,实际不应该有，而是通过订单的支付信息进行查询TODO
     */
    @NotBlank(message = "支付方式[payType]不能为空")
    private String payType;
}
