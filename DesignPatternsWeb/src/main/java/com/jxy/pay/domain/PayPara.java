package com.jxy.pay.domain;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName OrderInfo
 * @Description 订单信息
 * @Author zheng
 * @Date 2022/10/11 11:07
 * @Version 1.0
 **/
@Data
public class PayPara {
    /**
     * 订单id
     */
    @NotBlank(message = "订单id[orderId]不能为空")
    private String orderId;
    /**
     * 订单总金额
     */
    @DecimalMin(value = "0.01", message = "订单金额[total]不能小于0.01")
    private BigDecimal total;
    /**
     * 支付方式
     */
    @NotBlank(message = "支付方式[payType]不能为空")
    private String payType;


}
