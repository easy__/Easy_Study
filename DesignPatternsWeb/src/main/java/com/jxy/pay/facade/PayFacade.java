package com.jxy.pay.facade;

import com.jxy.pay.domain.PayPara;
import com.jxy.pay.domain.RefundPara;
import com.jxy.pay.factory.PayStrategyFactory;
import com.jxy.pay.strategy.PayContext;

/**
 * @ClassName PayFacade
 * @Description 支付门面
 * @Author zheng
 * @Date 2022/10/12 17:50
 * @Version 1.0
 **/
public class PayFacade {
    /**
     * @return java.lang.String
     * @Description 付款门面接口
     * @Date 2022/10/12 17:53
     * @Param [payPara]
     **/
    public static String pay(PayPara payPara) {
        PayContext payContext = new PayContext(PayStrategyFactory.getPayStrategy(payPara.getPayType()));
        return payContext.pay(payPara);
    }

    public static String refund(RefundPara refundPara) {
        PayContext payContext = new PayContext(PayStrategyFactory.getPayStrategy(refundPara.getPayType()));
        return payContext.refund(refundPara);
    }
}
