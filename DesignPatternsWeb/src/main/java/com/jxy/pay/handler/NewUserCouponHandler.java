package com.jxy.pay.handler;

import cn.hutool.core.util.RandomUtil;
import com.jxy.pay.domain.UserInfo;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;

/**
 * @ClassName NewUserCouponHandler
 * @Description TODO
 * @Author zheng
 * @Date 2022/10/20 11:07
 * @Version 1.0
 **/
@Slf4j
public class NewUserCouponHandler extends CouponHandler {
    @Override
    public void process(UserInfo userInfo) {
        if (!userInfo.isNewUser()) {
            log.info("您不是新用户，不能领取该优惠券。");
            throw new RuntimeException("您不是新用户，不能领取该优惠券。");
        } else if (nextHandler != null) {
            nextHandler.process(userInfo);
        }
    }
}
