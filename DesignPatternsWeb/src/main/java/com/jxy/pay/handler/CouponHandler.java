package com.jxy.pay.handler;

import com.jxy.pay.domain.UserInfo;

/**
 * @ClassName CouponHandler
 * @Description 优惠券领取
 * @Author zheng
 * @Date 2022/10/20 11:01
 * @Version 1.0
 **/
public abstract class CouponHandler {
    protected CouponHandler nextHandler;

    public final void setNextHandler(CouponHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    /**
     * @return java.lang.String
     * @Description 优惠券领取
     * @Date 2022/10/20 11:04
     * @Param [userInfo]
     **/
    public abstract void process(UserInfo userInfo);
}
