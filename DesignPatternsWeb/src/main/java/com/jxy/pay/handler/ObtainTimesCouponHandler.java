package com.jxy.pay.handler;

import cn.hutool.core.util.RandomUtil;
import com.jxy.pay.domain.UserInfo;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName ObtainTimesCouponHandler
 * @Description 获取次数校验
 * @Author zheng
 * @Date 2022/10/20 11:07
 * @Version 1.0
 **/
@Slf4j
public class ObtainTimesCouponHandler extends CouponHandler {
    @Override
    public void process(UserInfo userInfo) {
        int times = RandomUtil.randomInt(0, 3);
        if (times > 1) {
            log.info("您已领取过，不能重复领取该优惠券。");
            throw new RuntimeException("您已领取过，不能重复领取该优惠券。");
        } else if (nextHandler != null) {
            nextHandler.process(userInfo);
        }
    }
}
