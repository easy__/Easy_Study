package com.jxy.pay.handler;

import cn.hutool.core.util.RandomUtil;
import com.jxy.pay.domain.UserInfo;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName CityCouponHandler
 * @Description TODO
 * @Author zheng
 * @Date 2022/10/20 11:07
 * @Version 1.0
 **/
@Slf4j
public class CityCouponHandler extends CouponHandler {

    @Override
    public void process(UserInfo userInfo) {
        if (!"北京".equals(userInfo.getCity())) {
            log.info("您所在的地区暂时不能领取该优惠券。");
            throw new RuntimeException("您所在的地区暂时不能领取该优惠券。");
        } else if (nextHandler != null) {
            nextHandler.process(userInfo);
        }
    }
}
