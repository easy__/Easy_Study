package com.jxy.pay.enums;

/**
 * @author zheng
 * @Description 支付方式枚举
 * @Date 2022/10/11 20:49
 * @Param
 * @return
 **/
public enum PayTypeEnum {
    //微信
    WECHAT_PAY("1", "com.jxy.pay.strategy.WechatPayStrategy"),
    //支付宝
    ALI_PAY("2", "com.jxy.pay.strategy.AliPayStrategy");

    /**
     * @return java.lang.String 全类名
     * @Description
     * @Date 2022/10/12 9:53
     * @Param [type] 类型
     **/
    public static String getClassFullName(String type) {
        for (PayTypeEnum value : PayTypeEnum.values()) {
            if (value.getType().equals(type)) {
                return value.getClassFullName();
            }
        }
        return "";
    }

    PayTypeEnum(String type, String classFullName) {
        this.type = type;
        this.classFullName = classFullName;
    }

    private final String type;
    private final String classFullName;

    public String getType() {
        return type;
    }


    public String getClassFullName() {
        return classFullName;
    }


}
