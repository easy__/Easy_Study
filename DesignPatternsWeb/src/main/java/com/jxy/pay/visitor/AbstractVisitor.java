package com.jxy.pay.visitor;

import com.jxy.pay.composite.AbstractItem;

/**
 * @ClassName AbstractVisitor
 * @Description 抽象的访问者类
 * @Author zheng
 * @Date 2022/10/24 11:34
 * @Version 1.0
 **/
public abstract class AbstractVisitor<T> {
    /**
     * @return void
     * @Description 访问者方法
     * @Date 2022/10/24 11:37
     * @Param [abstractItem]
     **/
    public abstract T visitor(AbstractItem abstractItem);
}
