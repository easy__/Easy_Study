package com.jxy.pay.visitor;

import com.jxy.pay.composite.AbstractItem;
import com.jxy.pay.composite.DeptItem;
import com.jxy.pay.mock.DeptDataMock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @ClassName DeptVisitor
 * @Description 新增部门
 * @Author zheng
 * @Date 2022/10/24 11:38
 * @Version 1.0
 **/
@Slf4j
public class DeptNameVisitor extends AbstractVisitor<DeptItem> {
    @Override
    public DeptItem visitor(AbstractItem abstractItem) {
        DeptItem root = (DeptItem) abstractItem;
        log.info("部门名称访问者：{}", root.getDeptName());
        List<DeptItem> children = root.getChildren();
        for (DeptItem child : children) {
            child.accept(this);
        }
        return null;
    }
}
