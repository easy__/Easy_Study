package com.jxy.pay.composite;

import com.jxy.pay.visitor.AbstractVisitor;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName DeptItem
 * @Description 部门节点
 * @Author zheng
 * @Date 2022/10/20 20:24
 * @Version 1.0
 **/
@Data
public class DeptItem extends AbstractItem {
    /**
     * 部门id
     */
    private int id;
    /**
     * 父部门id
     */
    private int pid;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 子部门列表
     */
    private List<DeptItem> children = new ArrayList<DeptItem>();

    @Override
    public AbstractItem add(AbstractItem abstractItem) {
        this.children.add((DeptItem) abstractItem);
        return abstractItem;
    }

    @Override
    public AbstractItem del(AbstractItem abstractItem) {
        DeptItem deptItem = (DeptItem) abstractItem;
        this.children = children.stream().filter(item -> item.getId() != deptItem.getId()).collect(Collectors.toList());
        return deptItem;
    }

    @Override
    public void accept(AbstractVisitor visitor) {
        visitor.visitor(this);
    }
}
