package com.jxy.pay.composite;

import com.jxy.pay.visitor.AbstractVisitor;

/**
 * @ClassName AbsctractItem
 * @Description 抽象节点
 * @Author zheng
 * @Date 2022/10/20 20:22
 * @Version 1.0
 **/
public abstract class AbstractItem {
    /**
     * @return void
     * @Description 新增一个节点
     * @Date 2022/10/20 20:23
     * @Param [abstractItem]
     **/
    public abstract AbstractItem add(AbstractItem abstractItem);

    /**
     * @return void
     * @Description 删除一个节点
     * @Date 2022/10/20 20:23
     * @Param [abstractItem]
     **/
    public abstract AbstractItem del(AbstractItem abstractItem);

    public abstract void accept(AbstractVisitor abstractVisitor );
}
