package com.jxy.pay.service;

import com.jxy.pay.domain.UserInfo;

public interface ICouponService {

    public void getCoupon(UserInfo userInfo);
}
