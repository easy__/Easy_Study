package com.jxy.pay.service;

import com.jxy.pay.domain.PayPara;
import com.jxy.pay.domain.RefundPara;
import org.springframework.stereotype.Service;

/**
 * @author zheng
 */

public interface IPayService {

    public String pay(PayPara payPara);

    public String refund(RefundPara refundPara);
}
