package com.jxy.pay.service.impl;

import com.jxy.pay.domain.PayPara;
import com.jxy.pay.domain.RefundPara;
import com.jxy.pay.facade.PayFacade;
import com.jxy.pay.factory.PayStrategyFactory;
import com.jxy.pay.service.IPayService;
import com.jxy.pay.strategy.PayContext;
import com.jxy.pay.strategy.WechatPayStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName PayService
 * @Description 支付
 * @Author zheng
 * @Date 2022/10/11 14:40
 * @Version 1.0
 **/
@Slf4j
@Service
public class PayService implements IPayService {
    @Override
    public String pay(PayPara payPara) {
        return PayFacade.pay(payPara);
    }

    @Override
    public String refund(RefundPara refundPara) {
        return PayFacade.refund(refundPara);
    }
}
