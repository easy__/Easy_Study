package com.jxy.pay.service.impl;

import com.jxy.pay.domain.UserInfo;
import com.jxy.pay.handler.CityCouponHandler;
import com.jxy.pay.handler.NewUserCouponHandler;
import com.jxy.pay.handler.ObtainTimesCouponHandler;
import com.jxy.pay.service.ICouponService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName CouponServiceImpl
 * @Description TODO
 * @Author zheng
 * @Date 2022/10/20 14:37
 * @Version 1.0
 **/
@Slf4j
@Service
public class CouponServiceImpl implements ICouponService {
    @Override
    public void getCoupon(UserInfo userInfo) {
        NewUserCouponHandler newUserCouponHandler = new NewUserCouponHandler();
        CityCouponHandler cityCouponHandler = new CityCouponHandler();
        ObtainTimesCouponHandler obtainTimesCouponHandler = new ObtainTimesCouponHandler();

        newUserCouponHandler.setNextHandler(cityCouponHandler);
        cityCouponHandler.setNextHandler(obtainTimesCouponHandler);
        newUserCouponHandler.process(userInfo);
        log.info("获取优惠券成功");
    }
}
