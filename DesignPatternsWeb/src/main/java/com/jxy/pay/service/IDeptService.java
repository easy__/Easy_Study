package com.jxy.pay.service;

import com.jxy.pay.composite.DeptItem;

public interface IDeptService {
    public DeptItem list();

    public DeptItem add(DeptItem deptItem);

    public DeptItem del(int id);

    public void print();
}
