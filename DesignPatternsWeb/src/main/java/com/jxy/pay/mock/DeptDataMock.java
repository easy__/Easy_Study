package com.jxy.pay.mock;

import com.jxy.pay.composite.DeptItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName DeptDataMock
 * @Description TODO
 * @Author zheng
 * @Date 2022/10/20 20:41
 * @Version 1.0
 **/
public class DeptDataMock {
    public static DeptItem rootItem = new DeptItem();

    static {
        rootItem.setId(1);
        rootItem.setPid(0);
        rootItem.setDeptName("极限元");
        List<DeptItem> children = new ArrayList<DeptItem>();
        DeptItem child = new DeptItem();
        child.setDeptName("北京分公司");
        child.setPid(1);
        child.setId(2);
        children.add(child);
        child = new DeptItem();
        child.setDeptName("杭州分公司");
        child.setPid(1);
        child.setId(3);
        children.add(child);
        rootItem.setChildren(children);
    }

}
