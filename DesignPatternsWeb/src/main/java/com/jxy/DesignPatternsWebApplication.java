package com.jxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesignPatternsWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(DesignPatternsWebApplication.class, args);
    }

}
