package com.jxy.voice.domain;

import lombok.Data;

import javax.validation.constraints.*;

/**
 * @ClassName TtsPara
 * @Description 合成参数
 * @Author zheng
 * @Date 2022/10/13 10:10
 * @Version 1.0
 **/
@Data
public class TtsPara {
    /**
     * 待合成文本
     */
    @NotBlank(message = "合成内容[text]不能为空")
    private String text;
    /**
     * 音量
     */
    @Min(value = 0, message = "音量[volume]的范围为0~100")
    @Max(value = 100, message = "音量[volume]的范围为0~100")
    private String volume;
    /**
     * 倍速
     */
    @DecimalMin(value = "0.1", message = "倍速[speed]的范围为0.1~5.0")
    @DecimalMax(value = "5.0", message = "倍速[speed]的范围为0.1~5.0")
    private float speed;
    /**
     * 声码器类型
     */
    @NotBlank(message = "声码器[vocoder]不能为空")
    private String vocoder;
    /**
     * 说话人编号
     */
    @NotNull(message = "说话人编号[speakerId]不能为空")
    private Integer speakerId;
}
