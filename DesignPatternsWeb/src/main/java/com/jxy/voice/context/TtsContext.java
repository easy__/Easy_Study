package com.jxy.voice.context;

import com.jxy.voice.domain.TtsPara;
import com.jxy.voice.strategy.TtsStrategy;

/**
 * @ClassName TtsContext
 * @Description 合成上下文
 * @Author zheng
 * @Date 2022/10/13 14:37
 * @Version 1.0
 **/
public class TtsContext {
    private final TtsStrategy ttsStrategy;

    public TtsContext(TtsStrategy ttsStrategy) {
        this.ttsStrategy = ttsStrategy;
    }

    public byte[] tts(TtsPara ttsPara) {
        return ttsStrategy.tts(ttsPara);
    }
}
