package com.jxy.voice.strategy;

import com.jxy.voice.domain.TtsPara;

/**
 * @ClassName TtsStrategy
 * @Description 合成策略
 * @Author zheng
 * @Date 2022/10/13 14:27
 * @Version 1.0
 **/
public interface TtsStrategy {
    /**
     * @return byte[]
     * @Description 语音合成接口
     * @Date 2022/10/13 14:29
     * @Param [ttsPara] [合成参数]
     **/
    public byte[] tts(TtsPara ttsPara);
}
