package com.jxy.voice.strategy;

import com.jxy.voice.domain.TtsPara;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName LPCNetTtsStrategy
 * @Description LPCNet声码器进行合成
 * @Author zheng
 * @Date 2022/10/13 14:30
 * @Version 1.0
 **/
@Slf4j
public class LPCNetTtsStrategy implements TtsStrategy {
    @Override
    public byte[] tts(TtsPara ttsPara) {
        log.info("使用LPCNet声码器进行合成。参数为：{}",ttsPara);
        return new byte[0];
    }
}
