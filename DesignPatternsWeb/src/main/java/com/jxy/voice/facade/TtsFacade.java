package com.jxy.voice.facade;

import com.jxy.voice.context.TtsContext;
import com.jxy.voice.domain.TtsPara;
import com.jxy.voice.facotry.TtsStrategyFactory;

/**
 * @ClassName TtsFacade
 * @Description 合成门面，提供唯一的外部接口
 * @Author zheng
 * @Date 2022/10/13 15:45
 * @Version 1.0
 **/
public class TtsFacade {
    /**
     * @return byte[]
     * @Description 合成接口
     * @Date 2022/10/13 15:49
     * @Param [ttsPara]
     **/
    public static byte[] tts(TtsPara ttsPara) {
        TtsContext ttsContext = new TtsContext(TtsStrategyFactory.getTtsStrategy(ttsPara.getVocoder()));
        return ttsContext.tts(ttsPara);
    }
}
