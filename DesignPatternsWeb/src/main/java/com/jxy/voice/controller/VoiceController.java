package com.jxy.voice.controller;

import com.jxy.pay.domain.AjaxResult;
import com.jxy.voice.domain.TtsPara;
import com.jxy.voice.service.TtsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @ClassName VoiceController
 * @Description 语音
 * @Author zheng
 * @Date 2022/10/13 9:58
 * @Version 1.0
 **/
@RestController
@RequestMapping("/voice")
public class VoiceController {

    private final TtsService ttsService;

    public VoiceController(TtsService ttsService) {
        this.ttsService = ttsService;
    }

    @PostMapping("/tts")
    public AjaxResult tts(@RequestBody @Valid TtsPara ttsPara) {
        byte[] bytes = ttsService.tts(ttsPara);
        return AjaxResult.success(bytes);
    }
}
