package com.jxy.voice.service.impl;

import com.jxy.voice.domain.TtsPara;
import com.jxy.voice.facade.TtsFacade;
import com.jxy.voice.service.TtsService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TTSServiceImpl
 * @Description 合成
 * @Author zheng
 * @Date 2022/10/13 10:02
 * @Version 1.0
 **/
@Service
public class TtsServiceImpl implements TtsService {

    @Override
    public byte[] tts(TtsPara ttsPara) {
        return TtsFacade.tts(ttsPara);
    }
}
