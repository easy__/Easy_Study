package com.jxy.voice.service;

import com.jxy.voice.domain.TtsPara;

/**
 * @Description 合成
 * @Date 2022/10/13 10:02
 * @Param
 * @return
 **/
public interface TtsService {
    public byte[] tts(TtsPara ttsPara);
}
