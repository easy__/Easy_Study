package com.jxy.voice.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * @ClassName TtsEnum
 * @Description 合成策略枚举
 * @Author zheng
 * @Date 2022/10/13 14:43
 * @Version 1.0
 **/
public enum TtsTypeEnum {
    //LPCNet
    LPCNet("0", "com.jxy.voice.strategy.LPCNetTtsStrategy"), //Melgan
    Melgan("1", "com.jxy.voice.strategy.MelganTtsStrategy");
    /**
     * 类型
     **/
    private final String type;
    /**
     * 类全名
     **/
    private final String classFullName;

    public String getType() {
        return type;
    }

    public String getClassFullName() {
        return classFullName;
    }

    TtsTypeEnum(String type, String classFullName) {
        this.type = type;
        this.classFullName = classFullName;
    }

    /**
     * @return java.lang.String
     * @Description 根据类型获取对应的合成策略全路径
     * @Date 2022/10/13 14:51
     * @Param [type]
     **/
    public static String getTtsClassFullName(String type) {
        return Arrays.stream(TtsTypeEnum.values()).filter(ttsTypeEnum -> ttsTypeEnum.getType().equals(type)).findFirst().map(TtsTypeEnum::getClassFullName).orElse("");
    }
}
