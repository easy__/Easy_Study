package com.jxy;

import cn.hutool.core.io.FileUtil;

import java.io.File;
import java.io.FileFilter;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @ClassName test
 * @Description TODO
 * @Author zheng
 * @Date 2022/10/11 19:44
 * @Version 1.0
 **/
public class test {

    public static void main(String[] args) {

        File txtFile = FileUtil.newFile("F:\\现网粗粒度标注\\4k-未分类标签数据\\自动化所2k\\zdh20221018.log");
        List<String> list = FileUtil.readLines(txtFile, Charset.defaultCharset());
        for (String s : list) {
            //System.out.println(s);
            String[] split = s.split("\\|");
            String fileName = split[2];
            String srcFlag = fileName.split("_")[1];
            String destFlag = split[8];
            String dirName = srcFlag + "_" + destFlag;
            FileUtil.copy("F:\\现网粗粒度标注\\4k-未分类标签数据\\自动化所2k\\split\\split" + File.separator + fileName, "F:\\现网粗粒度标注\\4k-未分类标签数据\\自动化所2k\\split\\分类后" + File.separator + dirName + File.separator + fileName, true);
        }
    }
}
