package com.jxy;

import cn.hutool.core.io.FileUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

/**
 * @ClassName Excel
 * @Description TODO
 * @Author zheng
 * @Date 2022/10/12 15:10
 * @Version 1.0
 **/
public class Excel {
    public static void main(String[] args) {
        ExcelReader reader = ExcelUtil.getReader("F:\\现网粗粒度标注\\4k-未分类标签数据\\中科极限元2k\\1-2000\\1-2000.xlsx", 1);
        List<List<Object>> read = reader.read(0, reader.getRowCount());
        for (List<Object> row : read) {
            //文件名称
            String fileName = String.valueOf(row.get(0));
            //特定人名称
            String tdrName = String.valueOf(row.get(1));
            //伪造标识
            String fakeFlag = String.valueOf(row.get(3));
            //是否为有效音频
            String flag = String.valueOf(row.get(4));
            //是否加噪音
            String noiseFlag = String.valueOf(row.get(5));
            String tdrNameLabel = String.format("%02d", Integer.parseInt(tdrName));
            StringBuffer label = new StringBuffer();
            label.append(tdrNameLabel).append("_");
            label.append(("真实".equals(fakeFlag) || "T".equals(fakeFlag)) ? "real" : "fake").append("_")
                    //声码器名称
                    .append("unknown").append("_")
                    //是否加噪音
                    .append(("嘈杂".equals(noiseFlag) || ("2".equals(noiseFlag))) ? "noise" : "clean").append("_")
                    //伪造方式
                    .append("unknown");
            List<File> files = FileUtil.loopFiles("F:\\现网粗粒度标注\\4k-未分类标签数据\\中科极限元2k\\0", new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.getName().contains(fileName);
                }
            });
            File file = files.get(0);
            String md5 = MD5.create().digestHex16(file);
            label.append("_").append(md5).append("_").append(file.getName());
            System.out.println(label.toString());
            FileUtil.copy(file, FileUtil.newFile("F:\\现网粗粒度标注\\4k-未分类标签数据\\中科极限元2k\\有效音频\\" + label.toString()), true);
            /*String oldFileName = row.get(0).toString();
            if (oldFileName.contains(".")){
                oldFileName=oldFileName.substring(0, oldFileName.indexOf("."));
            }
            System.out.println(oldFileName);
            System.out.println(row);*/
        }
    }
}
