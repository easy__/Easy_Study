package com.easy.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @ClassName JdkProxy
 * @Description
 * @Author zheng
 * @Date 2021/12/22 10:24
 * @Version 1.0
 **/
public class JdkInvocationHandler implements InvocationHandler {
    /**
     * 被代理类
     */
    private final Object target;

    public JdkInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("jdk动态代理执行前");
        Object invoke = method.invoke(target, args);
        System.out.println("jdk动态代理执行后");
        return invoke;
    }
}
