package com.easy.proxy;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @ClassName CGLibMethodInterceptor
 * @Description TODO
 * @Author zheng
 * @Date 2021/12/22 15:02
 * @Version 1.0
 **/
public class CGLibMethodInterceptor implements MethodInterceptor {

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        System.out.println("CGLIB动态代理执行前");
        Object o1 = methodProxy.invokeSuper(o, args);
        System.out.println("CGLIB动态代理执行后");
        return o1;
    }
}
