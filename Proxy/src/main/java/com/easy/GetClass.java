package com.easy;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @ClassName GetClass
 * @Description 获取 Class 对象的四种方式
 * @Author zheng
 * @Date 2021/12/21 19:44
 * @Version 1.0
 **/
public class GetClass {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Class<?> aClass = Class.forName("com.easy.ClassTest");
        Class<?> aClass1 = ClassLoader.getSystemClassLoader().loadClass("com.easy.ClassTest");
        ClassTest classTest = new ClassTest();
        Class<? extends ClassTest> clazz = classTest.getClass();
        ClassTest testObj = clazz.newInstance();
        Class<ClassTest> classTestClass = ClassTest.class;
        System.out.println(aClass);
        System.out.println(aClass1);
        System.out.println(clazz);
        System.out.println(classTestClass);

        Field filed = clazz.getDeclaredField("filed");
        filed.setAccessible(true);
        filed.set(classTest, "1111");

        //获取所有的方法
        for (Method declaredMethod : clazz.getDeclaredMethods()) {
            System.out.println("类中定义的方法：" + declaredMethod.getName());
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(classTest);
        }
        //获取所有的字段
        for (Field declaredField : clazz.getDeclaredFields()) {
            System.out.println("类中定义的方法：" + declaredField.getName());
        }


    }
}
