import java.util.concurrent.*;

/**
 * @ClassName ThreadPoolTest
 * @Description TODO
 * @Author zheng
 * @Date 2021/12/27 10:31
 * @Version 1.0
 **/
public class ThreadPoolTest {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        Executors.newCachedThreadPool();
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(3);
        scheduledExecutorService.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("1");
            }
        }, 1, TimeUnit.SECONDS);
    }

}
