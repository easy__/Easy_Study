import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ClassName ThreadLoaclTest
 * @Description TODO
 * @Author zheng
 * @Date 2022/1/18 16:13
 * @Version 1.0
 **/
public class ThreadLoaclTest {

    public static final ThreadLocal<DateFormat> threadLocal = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    public static void main(String[] args) {
        System.out.println(ThreadLoaclTest.threadLocal.get().format(new Date()));
        ThreadLocal threadLocal = new ThreadLocal();
        threadLocal.set("111");
        System.out.println(threadLocal.get());
    }
}
