import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName MaxWeight
 * @Description 0-1背包
 * @Author zheng
 * @Date 2021/9/22 16:00
 * @Version 1.0
 **/
public class MaxWeight {
    int total = 0;

    public void maxWeight(int weight, int[] data, int[] choose) {
        if (total >= weight) {
            return;
        }
        for (int i = 0; i < data.length; i++) {
            if (choose[i] != 0) {
                continue;
            }
            if (weight(choose) + data[i] > weight) {
                //结束
                return;
            } else {
                choose[i] = data[i];
                maxWeight(weight, data, choose);
            }
        }
    }

    private int weight(int[] choose) {
        int total=0;
        for (int i : choose) {
            total+=i;
        }
        return total;
    }
}
