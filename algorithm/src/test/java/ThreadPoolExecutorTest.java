import java.time.LocalDateTime;
import java.util.concurrent.*;

/**
 * @ClassName ThreadPoolExecutorTest
 * @Description TODO
 * @Author zheng
 * @Date 2021/12/16 15:59
 * @Version 1.0
 **/
public class ThreadPoolExecutorTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(3, 3, 10, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10));
        for (int i = 0; i < 100; i++) {
            Future<String> future = threadPoolExecutor.submit(() -> {
                LocalDateTime localDateTime = LocalDateTime.now();
                //System.out.println(localDateTime);
                Thread.sleep(100);
                return localDateTime.toString();
            });
            System.out.println(future.get());
            System.out.println("任务数量是："+threadPoolExecutor.getTaskCount());
            System.out.println("完成的任务数量是："+threadPoolExecutor.getCompletedTaskCount());
        }
    }
}
