import java.util.Arrays;

/**
 * @ClassName MaxValue
 * @Description TODO
 * @Author zheng
 * @Date 2021/10/11 15:25
 * @Version 1.0
 **/
public class MaxValue {
    /**
     * @return int
     * @Description //错误写法1
     * 正序遍历背包容量，会导致同一个物品被放入多次
     * @Date 2021/10/11 15:52
     * @Param [bagWeight, weight, value]
     **/
    /*public int maxValue(int bagWeight, int[] weight, int[] value) {
        int[] dp = new int[bagWeight + 1];
        for (int i = 0; i < weight.length; i++) {
            for (int j = weight[i]; j <= bagWeight; j++) {
                dp[j] = Math.max(dp[j], dp[j - weight[i]] + value[i]);
                System.out.println("背包容量为" + j + "时，最大价值：" + dp[j]);
            }
            System.out.println("-----------------------------");
        }
        return dp[bagWeight];
    }*/
    public int maxValue(int bagWeight, int[] weight, int[] value) {
        int[] dp = new int[bagWeight + 1];
        for (int i = 0; i < weight.length; i++) {
            for (int j = bagWeight; j>=weight[i]; j--) {
                dp[j] = Math.max(dp[j], dp[j - weight[i]] + value[i]);
                System.out.println("背包容量为" + j + "时，最大价值：" + dp[j]);
            }
            System.out.println(Arrays.toString(dp));
            System.out.println("-----------------------------");
        }
        return dp[bagWeight];
    }

    public static void main(String[] args) {
        MaxValue maxValue = new MaxValue();
        System.out.println(maxValue.maxValue(4, new int[]{1, 3, 4}, new int[]{15, 20, 30}));
    }
}
