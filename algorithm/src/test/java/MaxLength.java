/**
 * @ClassName maxLength
 * @Description TODO
 * @Author zheng
 * @Date 2021/9/10 15:00
 * @Version 1.0
 **/
public class MaxLength {
    public int getStart(String s, String m) {
        int sLen = s.length();
        int mLen = m.length();
        int i = 0;
        int j = 0;
        while (i < sLen && j < mLen) {
            if (s.charAt(i) == m.charAt(j)) {
                i++;
                j++;
            } else {
                i = i - j + 1;
                j = 0;
            }
        }
        if (j == mLen) {
            return i - j;
        }
        return -1;
    }

    public static void main(String[] args) {
        String s = "BBCABCDABABCDABCDABDE";
        String m = "ABCDABD";
        MaxLength maxLength = new MaxLength();
        System.out.println(maxLength.getStart(s, m));
    }
}
