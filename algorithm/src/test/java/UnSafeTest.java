import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @ClassName UnSafeTest
 * @Description TODO
 * @Author zheng
 * @Date 2022/1/13 11:10
 * @Version 1.0
 **/
public class UnSafeTest {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        Field f = Unsafe.class.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        Unsafe unsafe = (Unsafe) f.get(null);
        String[] test = new String[]{"0", "1", "2", "3"};
        //每一个对象头的长度
        int arrayIndexScale = unsafe.arrayIndexScale(String[].class);
        //对象的起始位置
        int arrayBaseOffset = unsafe.arrayBaseOffset(String[].class);
        System.out.println(unsafe.getObject(test, arrayBaseOffset + arrayIndexScale * 3));
        AtomicInteger integer = new AtomicInteger(2);
        System.out.println(integer.compareAndSet(1, 1));
        System.out.println(integer.get());
        System.out.println("==============================");
        AtomicReference<Integer> integerAtomicReference = new AtomicReference<>();
        integerAtomicReference.set(1);
        System.out.println(integerAtomicReference.compareAndSet(1, 2));
        System.out.println(integerAtomicReference.compareAndSet(2, 1));
        System.out.println(integerAtomicReference.get());
        System.out.println("==============================");
        AtomicStampedReference<Integer> integerAtomicStampedReference = new AtomicStampedReference<Integer>(1, 1);
        System.out.println(integerAtomicStampedReference.compareAndSet(1, 2, integerAtomicStampedReference.getStamp(), integerAtomicStampedReference.getStamp() + 1));
        System.out.println(integerAtomicStampedReference.getReference());

        ConcurrentHashMap<Integer, Integer> integerConcurrentHashMap = new ConcurrentHashMap<Integer, Integer>();
        ConcurrentLinkedQueue<Integer> concurrentLinkedQueue = new ConcurrentLinkedQueue<>();
        for (int i = 0; i < 10; i++) {
            concurrentLinkedQueue.add(i);
            System.out.println(concurrentLinkedQueue.size());
        }

        concurrentLinkedQueue.poll();

        System.out.println("弹出数据后"+concurrentLinkedQueue.size());

    }
}
