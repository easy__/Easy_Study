import cn.hutool.core.io.FileUtil;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class TimeAvg {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\zheng\\Desktop\\time.txt");
        List<String> list = FileUtil.readLines(file, Charset.defaultCharset());
        OptionalDouble average = list.stream().map(s -> {
            return s.substring((s.indexOf("cost:") + 5), s.lastIndexOf(" ")).trim();
        }).mapToInt(Integer::parseInt).average();
        System.out.println(average.getAsDouble());
    }
}
