package com.easy.stream;

import java.util.stream.Stream;

/**
 * @ClassName StreamForEach
 * @Description TODO
 * @Author zheng
 * @Date 2021/12/13 14:53
 * @Version 1.0
 **/
public class StreamSorted {
    public static void main(String[] args) {
        Stream.of("1", "3", "4", "5", "1", "2", "12").map(Integer::parseInt)
                .sorted((a,b)->b-a).forEach(System.out::println);
    }
}