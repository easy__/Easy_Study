package com.easy.stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName StreamCollect
 * @Description TODO
 * @Author zheng
 * @Date 2021/12/13 17:53
 * @Version 1.0
 **/
public class StreamCollect {
    public static void main(String[] args) {
        //数组
        List<String> list = Stream.of("ab", "aa", "bb", "aa", "cc", "ff").collect(Collectors.toList());
        System.out.println(list);
        //set
        Set<String> set = Stream.of("ab", "aa", "bb", "aa", "cc", "ff").collect(Collectors.toSet());
        System.out.println(set);
        //新数组
        String[] strings = Stream.of("ab", "aa", "bb", "aa", "cc", "ff").toArray(String[]::new);
        System.out.println(Arrays.toString(strings));
        //最大值
        Optional<Integer> max = Stream.of(12, 3, 4, 55, 3122, 11).max(Comparator.comparingInt(a -> a));
        System.out.println("最大值："+max.get());
        //最小值
        Optional<Integer> min = Stream.of(12, 3, 4, 55, 3122, 11).min(Comparator.comparingInt(a -> a));
        System.out.println("最小值："+min.get());
        //join,以横线隔开
        String collect = Stream.of("ab", "aa", "bb", "aa", "cc", "ff").collect(Collectors.joining("-"));
        System.out.println("合并："+collect);
        //#开头，？结尾
        String collect1 = Stream.of("ab", "aa", "bb", "aa", "cc", "ff").collect(Collectors.joining("-","#","?"));
        System.out.println("合并："+collect1);
        //分组
        Map<String, List<String>> group = Stream.of("ab", "aa", "bb", "aa", "cc", "ff").collect(Collectors.groupingBy(a -> a));
        System.out.println(group.toString());
        //Stream.of("ab", "aa", "bb", "aa", "cc", "ff").collect(Collectors.toMap(val,obj->{obj}));
    }
}
