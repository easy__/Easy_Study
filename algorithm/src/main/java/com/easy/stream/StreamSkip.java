package com.easy.stream;

import java.util.Arrays;

/**
 * @ClassName StreamForEach
 * @Description TODO
 * @Author zheng
 * @Date 2021/12/13 14:53
 * @Version 1.0
 **/
public class StreamSkip {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 4, 5, 2, 4, 51, 32};
        Arrays.stream(arr).filter(num -> num > 3).skip(2).forEach(System.out::println);
    }
}