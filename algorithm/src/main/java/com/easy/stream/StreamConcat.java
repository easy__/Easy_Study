package com.easy.stream;

import java.util.stream.Stream;

/**
 * @ClassName StreamForEach
 * @Description TODO
 * @Author zheng
 * @Date 2021/12/13 14:53
 * @Version 1.0
 **/
public class StreamConcat {
    public static void main(String[] args) {
       Stream.concat(Stream.of(1,2,4),Stream.of(3,4,67)).forEach(System.out::println);
    }
}