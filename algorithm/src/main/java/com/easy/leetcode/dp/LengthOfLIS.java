package com.easy.leetcode.dp;

//300. 最长递增子序列
public class LengthOfLIS {
    public int lengthOfLIS(int[] nums) {
        //dp定义：以索引i结尾的最长递增子序列长度
        //状态转移方程：
        //此处需特别注意，不是往前找一个小于nums[i]的值，让dp[x]+1,而是找前面所有值中小于nums[i]的值中的最大值+1
        int len = nums.length;
        int[] dp = new int[len];
        int max = 1;
        //初始化特殊值
        dp[0] = 1;
        for (int i = 1; i < len; i++) {
            dp[i] = 1;
            for (int j = i; j >= 0; j--) {
                if (nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[j] + 1, dp[i]);
                }
            }
            max = Math.max(max, dp[i]);
        }
        return max;
    }
}
