package com.easy.leetcode.dp;

//整数拆分
public class IntegerBreak343 {
    public int integerBreak(int n) {
        //dp[i] ,拆分整数i 的最大乘积
        int[] dp = new int[n + 1];
        //初始化
        dp[2] = 1;
        for (int i = 3; i <= n; i++) {
            for (int j = 1; j < i - 1; j++) {
                //推导公式，最大乘积来源于以下两种可能，拆分成两个数j*(i-j)，拆分成多个数：j*dp[i-j],相当于把i-j继续拆分，求得最大值
                dp[i] = Math.max(dp[i], Math.max(j * (i - j), j * dp[i - j]));
            }
        }
        return dp[n];
    }
}
