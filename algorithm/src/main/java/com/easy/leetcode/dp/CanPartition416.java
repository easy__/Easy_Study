package com.easy.leetcode.dp;
//416. 分割等和子集
public class CanPartition416 {
    public boolean canPartition(int[] nums) {
        //背包容量 为sum的一半
        int sum=0;
        for (int num : nums) {
            sum+=num;
        }
        if (sum%2!=0){
            return false;
        }else {
            sum=sum>>1;
        }
        //dp 定义：背包容量为j时，物品的最大价值
        int[] dp = new int[sum+1];
        //遍历物品
        for (int i = 0; i < nums.length; i++) {
            //遍历背包，背包容量大于等于物品重量
            for (int j = sum; j >= nums[i]; j--) {
                dp[j]=Math.max(dp[j],dp[j-nums[i]]+nums[i]);
            }
        }
       return dp[sum]==sum;
    }
}
