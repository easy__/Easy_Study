package com.easy.leetcode.dp;

//62. 不同路径
public class UniquePaths62 {
    public int uniquePaths(int m, int n) {
        //dp[i][j]到达i,j位置的不同路径条数
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                //特殊值
                if (i == 0 || j == 0) {
                    dp[i][j] = 1;
                } else {
                    //状态转移方程
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                }
            }
        }
        return dp[m - 1][n - 1];
    }
}
