package com.easy.leetcode;

/**
 * @ClassName IntToRoman12
 * @Description 12. 整数转罗马数字
 * @Author zheng
 * @Date 2021/10/18 21:53
 * @Version 1.0
 **/
public class IntToRoman12 {
    public String intToRoman(int num) {
        StringBuilder sb = new StringBuilder();
        int[] values = new int[]{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] sy = new String[]{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV","I"};
        for (int i = 0; i < values.length; i++) {
            while (num >= values[i]) {
                sb.append(sy[i]);
                num -= values[i];
            }
        }
        return sb.toString();
    }
}
