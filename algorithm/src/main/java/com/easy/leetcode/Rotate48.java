package com.easy.leetcode;

import java.util.Arrays;

/**
 * @ClassName Rotate48
 * @Description 48. 旋转图像
 * @Author zheng
 * @Date 2021/12/2 19:30
 * @Version 1.0
 **/
public class Rotate48 {
    public void rotate(int[][] matrix) {
        int len = matrix.length;
        //横向对折，互换数据
        for (int i = 0; i < len / 2; i++) {
            for (int j = 0; j < len; j++) {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[len - 1 - i][j];
                matrix[len - 1 - i][j] = tmp;
            }
        }
        //斜对角线对折，互换数据
        for (int i = 0; i < len; i++) {
            for (int j = i; j < len; j++) {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = tmp;
            }
        }



        for (int[] ints : matrix) {
            System.out.println(Arrays.toString(ints));
        }
    }

    public static void main(String[] args) {
        Rotate48 rotate48 = new Rotate48();
        rotate48.rotate(new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});
    }
}
