package com.easy.leetcode.bit;

/**
 * 136. 只出现一次的数字
 */
public class SingleNumber136 {
    public int singleNumber(int[] nums) {
        int res=0;
        for (int num : nums) {
            res=res^num;
        }
        return res;
    }
}
