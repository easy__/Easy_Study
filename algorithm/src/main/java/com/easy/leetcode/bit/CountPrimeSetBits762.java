package com.easy.leetcode.bit;

/**
 * 762. 二进制表示中质数个计算置位
 * 看题要看全，题意
 * L, R 是 L <= R 且在 [1, 10^6] 中的整数。
 * R - L 的最大值为 10000。
 *
 * R 最大值为10^6次方，不超过2的20次方，即最多有20个1
 * 那么求的是20以内的质数
 */
public class CountPrimeSetBits762 {
    public int countPrimeSetBits(int left, int right) {
        int count=0;
        for (int i = left; i <=right ; i++) {
              //1的个数
              int num=  Integer.bitCount(i);
            if (num==1||num==2||num==3||num==5||num==7||num==11||num==13||num==17||num==19) {
                count++;
            }
        }
        return count;
    }
}
