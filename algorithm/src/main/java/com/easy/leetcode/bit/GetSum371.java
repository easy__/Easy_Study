package com.easy.leetcode.bit;

/**
 * 371. 两整数之和
 * 还需要再思考
 */
public class GetSum371 {
    public int getSum(int a, int b) {
        while (b!=0){
            //无进位相加
            int tmp =a^b;
            //进位
            b=(a&b)<<1;
            a=tmp;
        }
        return a;
    }
}
