package com.easy.leetcode.bit;

/**
 * 2 的幂
 */
public class IsPowerOfTwo231 {
    public boolean isPowerOfTwo(int n) {
        return n>0&& (n&(n-1))==0;
    }
}
