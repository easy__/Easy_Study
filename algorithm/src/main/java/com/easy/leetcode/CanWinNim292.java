package com.easy.leetcode;

/**
 * 292. Nim 游戏
 */
public class CanWinNim292 {
    public boolean canWinNim(int n) {
        return n%4!=0;
    }
}
