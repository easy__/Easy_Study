package com.easy.leetcode;

import java.util.*;

/**
 * @ClassName LetterCombinations17
 * @Description TODO
 * @Author zheng
 * @Date 2021/10/25 22:00
 * @Version 1.0
 **/
public class LetterCombinations17 {
    public List<String> letterCombinations(String digits) {
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        map.put("2", Arrays.asList("a", "b", "c"));
        map.put("3", Arrays.asList("d", "e", "f"));
        map.put("4", Arrays.asList("g", "h", "i"));
        map.put("5", Arrays.asList("j", "k", "l"));
        map.put("6", Arrays.asList("m", "n", "o"));
        map.put("7", Arrays.asList("p", "q", "r", "s"));
        map.put("8", Arrays.asList("t", "u", "v"));
        map.put("9", Arrays.asList("w", "x", "y", "z"));
        return helper(map, digits, digits.length()-1);
    }

    public List<String> helper(Map<String, List<String>> numMap, String digits, int start) {
        if (digits.length() == 0) {
            return new ArrayList<>();
        }
        List<String> chars = numMap.get(String.valueOf(digits.charAt(start)));
        if (start == 0) {
            return chars;
        } else {
            List<String> res = helper(numMap, digits, start - 1);
            List<String> tmp = new ArrayList<>();
            res.forEach(str -> {
                for (String aChar : chars) {
                    tmp.add(str + aChar);
                }
            });
            return tmp;
        }
    }

    public static void main(String[] args) {
        LetterCombinations17 letterCombinations17 = new LetterCombinations17();
        List<String> lst = letterCombinations17.letterCombinations("234");
        lst.forEach(System.out::println);
    }
}
