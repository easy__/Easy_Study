package com.easy.leetcode;

import java.util.HashMap;

/**
 * @ClassName isValidSudoku36
 * @Description 36. 有效的数独
 * @Author zheng
 * @Date 2021/9/6 14:45
 * @Version 1.0
 **/
public class IsValidSudoku36 {
    public boolean isValidSudoku(char[][] board) {
        int rowLength = board.length;
        int columnLength = board[0].length;
        boolean[][] row = new boolean[rowLength][columnLength];
        boolean[][] column = new boolean[rowLength][columnLength];
        boolean[][] box = new boolean[rowLength][columnLength];
        for (int i = 0; i < rowLength; i++) {
            for (int j = 0; j < columnLength; j++) {
                if (board[i][j] != '.') {
                    int num = board[i][j] - '1';
                    int index = (i / 3) * 3 + j / 3;
                    if (row[i][num] || column[j][num] || box[index][num]) {
                        return false;
                    } else {
                        row[i][num] = true;
                        column[j][num] = true;
                        box[index][num] = true;
                    }
                }
            }
        }
        return true;
    }
}
