package com.easy.leetcode;

import java.util.ArrayList;
import java.util.List;

public class SpiralOrder54 {
    public List<Integer> spiralOrder(int[][] matrix) {
        int row= matrix.length;
        int col =matrix[0].length;
        int loop=Math.min(row,col)/2;
        int startRow=0;
        int startCol=0;
        int offset=1;
        List<Integer> res= new ArrayList<>();
        //总个数
        int total = row * col;
        while (loop>0){
            int tmpStartRow=startRow;
            int tmpStartCol=startCol;
            for (;tmpStartCol<col-offset;tmpStartCol++){
                res.add(matrix[tmpStartRow][tmpStartCol]);
            }
            for (;tmpStartRow<row-offset;tmpStartRow++){
                res.add(matrix[tmpStartRow][tmpStartCol]);
            }
            for (;tmpStartCol>=offset;tmpStartCol--){
                res.add(matrix[tmpStartRow][tmpStartCol]);
            }
            for (;tmpStartRow>=offset;tmpStartRow--){
                res.add(matrix[tmpStartRow][tmpStartCol]);
            }
            offset++;
            startRow++;
            startCol++;
            loop--;
        }
        if (res.size() < total) {
            if (row > col) {
                for (; startRow <= row &&res.size() < total; startRow++) {
                    res.add(matrix[startRow][startCol]);
                }
            } else {
                for (; startCol <= col && res.size() < total; startCol++) {
                    res.add(matrix[startRow][startCol]);
                }
            }
        }
        return res;
    }
}
