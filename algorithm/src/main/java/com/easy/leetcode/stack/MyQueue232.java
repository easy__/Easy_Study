package com.easy.leetcode.stack;

import java.util.Stack;

/**
 * @ClassName MyQueue
 * @Description 232. 用栈实现队列
 * @Author zheng
 * @Date 2021/9/6 11:35
 * @Version 1.0
 **/
public class MyQueue232 {

    Stack<Integer> in = null;
    Stack<Integer> out = null;

    /**
     * Initialize your data structure here.
     */
    public MyQueue232() {
        in = new Stack<Integer>();
        out = new Stack<Integer>();
    }

    /**
     * Push element x to the back of queue.
     */
    public void push(int x) {
        in.push(x);
    }

    /**
     * Removes the element from in front of queue and returns that element.
     */
    public int pop() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
        return out.pop();
    }

    /**
     * Get the front element.
     */
    public int peek() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
        return out.peek();
    }

    /**
     * Returns whether the queue is empty.
     */
    public boolean empty() {
        return in.isEmpty() && out.isEmpty();
    }
}
