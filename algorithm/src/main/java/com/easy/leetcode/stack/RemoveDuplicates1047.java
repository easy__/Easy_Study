package com.easy.leetcode.stack;

import java.util.Stack;

/**
 * @ClassName RemoveDuplicates
 * @Description 1047. 删除字符串中的所有相邻重复项
 * @Author zheng
 * @Date 2021/9/8 11:57
 * @Version 1.0
 **/
public class RemoveDuplicates1047 {
    public String removeDuplicates(String s) {
        Stack<Character> stack = new Stack<>();
        char[] chars = s.toCharArray();
        for (char c : chars) {
            if (!stack.isEmpty() && stack.peek().equals(c)) {
                stack.pop();
            } else {
                stack.push(c);
            }
        }
        String sb = "";
        while (!stack.isEmpty()) {
            sb = stack.pop() + sb;
        }
        return sb;
        //return stack.stream().map(Object::toString).reduce("", String::concat);
    }
}
