package com.easy.leetcode.stack;

import java.util.Stack;

/**
 * @ClassName CalPoints682
 * @Description 682. 棒球比赛
 * @Author zheng
 * @Date 2021/9/7 20:55
 * @Version 1.0
 **/
public class CalPoints682 {
    public int calPoints(String[] ops) {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < ops.length; i++) {
            if (ops[i].equals("C")) {
                stack.pop();
            } else if (ops[i].equals("D")) {
                stack.push(stack.peek() * 2);
            } else if (ops[i].equals("+")) {
                int pre = stack.pop();
                int sum = pre + stack.peek();
                stack.push(pre);
                stack.push(sum);
            } else {
                stack.push(Integer.parseInt(ops[i]));
            }
        }
        int sum = 0;
        while (!stack.isEmpty()) {
            sum += stack.pop();
        }
        return sum;
    }

    public static void main(String[] args) {
        String[] arr= new String[]{
                "5","2","C","D","+"
        };
        CalPoints682 calPoints682 = new CalPoints682();
        calPoints682.calPoints(arr);
    }
}
