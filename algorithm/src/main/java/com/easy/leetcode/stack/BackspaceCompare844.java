package com.easy.leetcode.stack;

import java.util.Stack;

/**
 * @ClassName BackspaceCompare844
 * @Description 844. 比较含退格的字符串
 * @Author zheng
 * @Date 2021/9/8 10:06
 * @Version 1.0
 **/
public class BackspaceCompare844 {
    public boolean backspaceCompare(String s, String t) {
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != '#') {
                stack.push(s.charAt(i));
            } else {
                if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
        }
        Stack<Character> stack1 = new Stack<Character>();
        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) != '#') {
                stack1.push(t.charAt(i));
            } else {
                if (!stack1.isEmpty()) {
                    stack1.pop();
                }
            }
        }
        if (stack1.size() != stack.size()) {
            return false;
        } else {
            while (!stack.isEmpty()) {
                if (!stack1.pop().equals(stack.pop())) {
                    return false;
                }
            }
        }
        return true;
    }
}
