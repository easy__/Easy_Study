package com.easy.leetcode.listNode;

import com.easy.leetcode.ListNode;

/**
 * @ClassName ListNode_206_ReverseList
 * @Description 206 反转链表
 * @Author zheng
 * @Date 2021/8/26 17:20
 * @Version 1.0
 **/
public class ReverseList206 {
    /**
     * @return com.easy.leetcode.listNode.ListNode
     * @Description 206 反转链表
     * @Date 2021/8/25 17:14
     * @Param [head]
     **/
    public ListNode reverseList(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode pre = null;
        while (head != null) {
            ListNode tmp = head.next;
            head.next = pre;
            pre = head;
            head = tmp;
        }
        return pre;
    }

    public ListNode reverseList1(ListNode head) {
        if (head==null||head.next==null) {
            return head;
        }
        ListNode cur= reverseList1(head.next);
        head.next.next=head;
        head.next=null;
        return cur;
    }

    public static void main(String[] args) {
        ListNode node = new ListNode(-1);
        ListNode cur = node;
        for (int i = 1; i < 6; i++) {
            cur.next= new ListNode(i);
            cur= cur.next;
        }
        ReverseList206 reverseList206 = new ReverseList206();
        node=reverseList206.reverseList1(node);
        while (node!=null){
            System.out.println(node.val);
            node=node.next;
        }
    }
}
