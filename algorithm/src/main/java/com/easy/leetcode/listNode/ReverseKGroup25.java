package com.easy.leetcode.listNode;

import com.easy.leetcode.ListNode;

import java.util.List;

/**
 * @ClassName ReverseKGroup25
 * @Description 25. K 个一组翻转链表
 * @Author zheng
 * @Date 2021/11/1 15:33
 * @Version 1.0
 **/
public class ReverseKGroup25 {

    public static ListNode reverseKGroup(ListNode head, int k) {
        ListNode res = new ListNode(-1);
        ListNode tmp = res;
        int count = 0;
        ListNode newHead = null;
        while (head != null) {
            if (count == 0) {
                newHead = head;
            }
            count++;
            if (count == k) {
                ListNode node = head.next;
                head.next = null;
                tmp.next = reverse(newHead);
                tmp = newHead;
                head = node;
                count = 0;
            } else {
                head = head.next;
            }
            if (count != 0) {
                tmp.next = newHead;
            }
        }
        return res.next;
    }

    //反转链表
    public static ListNode reverse(ListNode head) {
        //前一个节点
        ListNode pre = null;
        while (head != null) {
            //保存节点
            ListNode cur = head;
            //下一个节点
            head = head.next;
            cur.next = pre;
            pre = cur;
        }
        return pre;
    }


    public static void main(String[] args) {
        ListNode head = new ListNode(-1);
        ListNode tmp = head;
        for (int i = 0; i < 3; i++) {
            tmp.next= new ListNode(i);
            tmp=tmp.next;
        }
        ListNode res= reverseKGroup(head.next,1);
        System.out.println(1111);
    }
}
