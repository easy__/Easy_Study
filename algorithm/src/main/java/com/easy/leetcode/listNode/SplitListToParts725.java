package com.easy.leetcode.listNode;

import com.easy.leetcode.ListNode;

/**
 * 725. 分隔链表
 */
public class SplitListToParts725 {
    public ListNode[] splitListToParts(ListNode head, int k) {
        //计算链表的长度
        ListNode tmp =head;
        ListNode[] listNodes = new ListNode[k];
       int len=0;
       while (tmp!=null){
           tmp=tmp.next;
           len++;
       }
       //计算链表每一段应该的长度
       int quotient =len/k;
       int remainder =len%k;
        ListNode curr = head;
        for (int i = 0; i < k&&curr!=null; i++) {
            //保存链表
            listNodes[i]=curr;
            //每一段链表的长度
            int size= i<remainder?quotient+1:quotient;
            //移动链表指针至每一段的结尾
            for (int j = 1; j <size ; j++) {
                curr=curr.next;
            }
            //断开链表
            ListNode next= curr.next;
            curr.next=null;
            curr=next;
        }
        return listNodes;
    }
}
