package com.easy.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * 383. 赎金信
 */
public class CanConstruct383 {
    public boolean canConstruct(String ransomNote, String magazine) {
        if (magazine.isEmpty()||ransomNote.length()<magazine.length()) {
            return false;
        }
        char[] magazineArray = magazine.toCharArray();
        Map<Character,Integer> map  = new HashMap<>();
        for (char c : magazineArray) {
           map.put(c,map.getOrDefault(c,0)+1);
        }
        char[] ransomNoteArray= ransomNote.toCharArray();
        for (char c : ransomNoteArray) {
            if (map.getOrDefault(c,0)>0){
                map.put(c,map.getOrDefault(c,0)-1);
            }else {
                return false;
            }
        }

        return true;
    }
}
