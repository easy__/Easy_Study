package com.easy.leetcode;
//55. 跳跃游戏
public class CanJump55 {
    public boolean canJump(int[] nums) {
        //最远能跳到的索引
        int max=0;
        for (int i = 0; i < nums.length; i++) {
            //起跳位置必须小于等于最远距离
            if (i<=max){
                //最远能跳到的索引为起跳位置加跳的距离 与max的最大值
                max=Math.max(max,i+nums[i]);
            }
            //最大值的索引大于等于数组的最后一个索引，则能跳到
            if (max>=nums.length-1){
                return true;
            }
        }
        return false;
    }
}
