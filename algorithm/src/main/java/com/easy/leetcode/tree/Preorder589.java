package com.easy.leetcode.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Preorder589
 * @Description 589. N 叉树的前序遍历
 * @Author zheng
 * @Date 2021/9/7 20:37
 * @Version 1.0
 **/
public class Preorder589 {
    public List<Integer> preorder(Node root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        res.add(root.val);
        List<Node> children = root.children;
        if (children != null) {
            for (int i = 0; i < children.size(); i++) {
                Node node = children.get(i);
                res.addAll(preorder(children.get(i)));
            }
        }
        return res;
    }
}
