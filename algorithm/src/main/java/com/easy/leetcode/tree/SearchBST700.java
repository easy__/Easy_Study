package com.easy.leetcode.tree;

/**
 * 700. 二叉搜索树中的搜索
 */
public class SearchBST700 {
    public TreeNode searchBST(TreeNode root, int val) {
        if (root==null) {
            return null;
        }
        if (val<root.val){
           return searchBST(root.left,val);
        }else if (val>root.val){
            return searchBST(root.right,val);
        }else {
            return root;
        }
    }
}
