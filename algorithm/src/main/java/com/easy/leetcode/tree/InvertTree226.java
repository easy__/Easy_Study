package com.easy.leetcode.tree;

/**
 * @ClassName InvertTree226
 * @Description 226. 翻转二叉树
 * @Author zheng
 * @Date 2021/9/9 19:39
 * @Version 1.0
 **/
public class InvertTree226 {
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode tmp = root.left;
        root.left = invertTree(root.right);
        root.right = invertTree(tmp);
        return root;
    }
}
