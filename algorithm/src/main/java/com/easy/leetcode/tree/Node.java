package com.easy.leetcode.tree;

import java.util.List;

/**
 * @ClassName Node
 * @Description TODO
 * @Author zheng
 * @Date 2021/9/7 20:37
 * @Version 1.0
 **/
public class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
}
