package com.easy.leetcode.tree;

/**
 * @ClassName TreeNode
 * @Description tree
 * @Author zheng
 * @Date 2021/9/6 16:45
 * @Version 1.0
 **/
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode() {
    }

    public TreeNode(int val) {
        this.val = val;
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

