package com.easy.leetcode.tree;

/**
 * @ClassName IsSymmetric101
 * @Description TODO
 * @Author zheng
 * @Date 2021/9/8 16:15
 * @Version 1.0
 **/
public class IsSymmetric101 {
    public boolean isSymmetric(TreeNode root) {
        return helper(root,root);
    }

    public boolean helper(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        } else if (left == null || right == null) {
            return false;
        } else {
            return left.val == right.val && helper(left.left, right.right) && helper(left.right, right.left);
        }
    }
}
