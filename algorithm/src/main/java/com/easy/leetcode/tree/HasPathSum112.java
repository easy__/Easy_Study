package com.easy.leetcode.tree;

/**
 * @ClassName HasPathSum112
 * @Description 112. 路径总和
 * @Author zheng
 * @Date 2021/9/9 20:26
 * @Version 1.0
 **/
public class HasPathSum112 {
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null) {
            return false;
        }
        if (root.left == null && root.right == null) {
            return root.val == targetSum;
        }
        return hasPathSum(root.left, targetSum - root.val) || hasPathSum(root.right, targetSum - root.val);
    }
}
