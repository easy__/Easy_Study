package com.easy.leetcode.tree;

/**
 * @ClassName MaxDepth
 * @Description 104. 二叉树的最大深度
 * @Author zheng
 * @Date 2021/9/8 16:08
 * @Version 1.0
 **/
public class MaxDepth104 {
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }
}
