package com.easy.leetcode.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Postorder590
 * @Description 590. N 叉树的后序遍历
 * @Author zheng
 * @Date 2021/9/7 20:51
 * @Version 1.0
 **/
public class Postorder590 {
    public List<Integer> postorder(Node root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        List<Node> children = root.children;
        if (children != null) {
            for (int i = 0; i < children.size(); i++) {
                Node node = children.get(i);
                res.addAll(postorder(children.get(i)));
            }
        }
        res.add(root.val);
        return res;
    }

}
