package com.easy.leetcode.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName InorderTraversal
 * @Description 94. 二叉树的中序遍历
 * @Author zheng
 * @Date 2021/9/6 17:00
 * @Version 1.0
 **/
public class InorderTraversal94 {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        res.addAll(inorderTraversal(root.left));
        res.add(root.val);
        res.addAll(inorderTraversal(root.right));
        return res;
    }
}
