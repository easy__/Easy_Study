package com.easy.leetcode.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName PreorderTraversal144
 * @Description 144. 二叉树的前序遍历
 * @Author zheng
 * @Date 2021/9/6 16:44
 * @Version 1.0
 **/
public class PreorderTraversal144 {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        res.add(root.val);
        res.addAll(preorderTraversal(root.left));
        res.addAll(preorderTraversal(root.right));
        return res;
    }

}
