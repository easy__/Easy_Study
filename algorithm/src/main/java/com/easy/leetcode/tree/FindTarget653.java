package com.easy.leetcode.tree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 653. 两数之和 IV - 输入 BST
 */
public class FindTarget653 {
    public boolean findTarget(TreeNode root, int k) {
        HashSet<Integer> set= new HashSet<>();
        return find(root,k,set);
    }
    public boolean find(TreeNode root, int k, HashSet<Integer> set){
        if (root==null){
            return false;
        }
        if (set.contains(k-root.val)){
            return true;
        }
        set.add(root.val);
        return find(root.left,k,set)||find(root.right,k,set);
    }
}
