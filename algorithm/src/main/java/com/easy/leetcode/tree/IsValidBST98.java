package com.easy.leetcode.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * 98. 验证二叉搜索树
 */

public class IsValidBST98 {
    public boolean isValidBST1(TreeNode root) {
        List<Integer> lst= inorder(root);
        for (int i = 1; i < lst.size(); i++) {
                if (lst.get(i-1)>lst.get(i)){
                return false;
            }
        }
        return true;
    }

    public List<Integer> inorder(TreeNode root){
        List<Integer> lst= new ArrayList<>();
        if(root==null){
            return lst;
        }
        lst.addAll(inorder(root.left));
        lst.add(root.val);
        lst.addAll(inorder(root.right));
        return lst;
    }

        int min =Integer.MIN_VALUE;
    int max= Integer.MAX_VALUE;
    public boolean isValidBST(TreeNode root) {
       return valid(root,min,max);
    }

    public boolean valid(TreeNode root,int min,int max){
        if (root==null){
            return true;
        }
        if (root.val>=max||root.val<=min){
            return false;
        }
        return valid(root.left,min,root.val)&&valid(root.right,root.val,max);

    }

}
