package com.easy.leetcode;

/**
 * @ClassName RomanToInt13
 * @Description TODO
 * @Author zheng
 * @Date 2021/10/19 10:04
 * @Version 1.0
 **/
public class RomanToInt13 {
    public int romanToInt(String s) {
        int[] values = new int[]{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] sy = new String[]{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int sum = 0;
        while (s.length() > 0) {
            for (int i = 0; i < sy.length; i++) {
                if (s.startsWith(sy[i])) {
                    sum += values[i];
                    s = s.substring(s.indexOf(sy[i])+sy[i].length());
                }
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        RomanToInt13 romanToInt13 = new RomanToInt13();
        System.out.println(romanToInt13.romanToInt("III")); ;
    }
}
