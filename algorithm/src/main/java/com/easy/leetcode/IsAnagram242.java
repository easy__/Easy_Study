package com.easy.leetcode;

/**
 * 242. 有效的字母异位词
 */
public class IsAnagram242 {
    public boolean isAnagram(String s, String t) {
        if (s.length()!=t.length()){
            return false;
        }
        int[] arr= new int[26];
        char[] chars= s.toCharArray();
        for (int i = 0; i <chars.length ; i++) {
            char c = chars[i];
            int index =  c-97;
            arr[index]=arr[index]+1;
        }
        char[] tchars = t.toCharArray();
        for (char tchar : tchars) {
            int index =  tchar-97;
            if (arr[index]>0){
                arr[index]=  arr[index]--;
            }else {
                return false;
            }
        }
        return true;
    }
}
