package com.easy.leetcode;

public class MaxWeight {
    int maxWeight=0;
    public void maxWeight(int[] items,int bagWeight,int i,int totalWeight){
        //退出条件
        //背包容量等于总的重量或者考察到最后一个了
        if (totalWeight==bagWeight||i==items.length) {
            maxWeight=Math.max(totalWeight,maxWeight);
            return;
        }
        //每个元素有两种选择
        //不放当前元素
        maxWeight(items,bagWeight,i+1,totalWeight);
        //存放当前元素，需判断是否大于背包容量
        if (totalWeight+items[i]<=bagWeight) {
            maxWeight(items,bagWeight,i+1,totalWeight+items[i]);
        }
    }
}
