package com.easy.leetcode.array;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName FindRelativeRanks506
 * @Description 506. 相对名次
 * @Author zheng
 * @Date 2021/12/3 9:29
 * @Version 1.0
 **/
public class FindRelativeRanks506 {
    public String[] findRelativeRanks(int[] score) {
        int len = score.length;
        //记录value对应的索引位置
        Map<Integer, Integer> map = new HashMap<>(16);
        for (int i = 0; i < len; i++) {
            map.put(score[i], i);
        }
        String[] result = new String[score.length];
        Arrays.sort(score);

        for (int i = 0; i < len; i++) {
            int index = map.get(score[i]);
            if (i == len - 1) {
                result[index] = "Gold Medal";
            } else if (i == len - 2) {
                result[index] = "Silver Medal";
            } else if (i == len - 3) {
                result[index] = "Bronze Medal";
            } else {
                result[index] = String.valueOf(len - i);
            }
        }
        return result;
    }
}
