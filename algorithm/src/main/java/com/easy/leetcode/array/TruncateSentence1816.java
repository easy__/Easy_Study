package com.easy.leetcode.array;

/**
 * @ClassName TruncateSentence1816
 * @Description 1816. 截断句子
 * @Author zheng
 * @Date 2021/12/6 9:39
 * @Version 1.0
 **/
public class TruncateSentence1816 {
    public String truncateSentence(String s, int k) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                k--;
            }
            if (k == 0) {
                return s.substring(0, i);
            }
        }
        return s;
    }

    public static void main(String[] args) {
        TruncateSentence1816 truncateSentence1816 = new TruncateSentence1816();
        System.out.println(truncateSentence1816.truncateSentence("chopper is not a tanuki",5));
    }
}
