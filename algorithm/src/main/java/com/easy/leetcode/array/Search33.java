package com.easy.leetcode.array;

public class Search33 {
    public static int search(int[] nums, int target) {
        int left=0;
        int right=nums.length;
        //左闭右开
        while (left<right){
            int mid =(left+right)/2;
            if (nums[mid]==target){
                return mid;
            }
            //中点左边有序
            if (nums[left]<nums[mid]){
                //目标值在左边区间
                if (target>=nums[left]&&target<nums[mid]){
                    right=mid;
                }else {
                    left=mid+1;
                }
            }else {//右边有序
                //目标值在右半区间
                if (target>nums[mid]&&target<=nums[right-1]){
                    left=mid+1;
                }else {
                    right=mid;
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(search(new int[]{3,1},3));
    }
}
