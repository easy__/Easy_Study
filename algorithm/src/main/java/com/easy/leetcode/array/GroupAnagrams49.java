package com.easy.leetcode.array;

import java.util.*;

/**
 * 49. 字母异位词分组
 *
 * 把每一个词的字母按序排列，重新构建一个字符串作为key,相同的key放在一起
 * 
 */
public class GroupAnagrams49 {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String,List<String>> map = new HashMap();
        for (String str : strs) {
         char[] chars= str.toCharArray();
         Arrays.sort(chars);
         String tmp = new String(chars);
         List<String> lst= map.getOrDefault(tmp,new ArrayList<String>());
         lst.add(str);
         map.put(tmp,lst);
        }
        return new ArrayList<>(map.values());
    }


}
