package com.easy.leetcode.array;

public class SearchInsert35 {
    public int searchInsert(int[] nums, int target) {
        return search(nums,0,nums.length-1,target);
    }

    public int search(int[] nums ,int left,int right,int target){
        int mid =0;
        while (left<=right){
            mid=left+(right-left)/2;
            if (nums[mid]>target){
                right=mid-1;
            }else if (nums[mid]<target){
                left=mid+1;
            }else {
                return mid;
            }
        }
        System.out.println(left+" right:"+right);

        return left;
    }

    public static void main(String[] args) {
        SearchInsert35 searchInsert35 = new SearchInsert35();
        int[] nums=new int[]{1,3,5,7,9,11,13,15,17,19 ,21,23,25,27};
        searchInsert35.search(nums,0,nums.length-1,8);
    }
}
