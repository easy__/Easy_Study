package com.easy.leetcode.array;

/**
 * @ClassName MatrixReshape566
 * @Description 566. 重塑矩阵
 * @Author zheng
 * @Date 2021/9/6 16:03
 * @Version 1.0
 **/
public class MatrixReshape566 {
    public int[][] matrixReshape(int[][] mat, int r, int c) {
        int row = mat.length;
        int column = mat[0].length;
        if (row * column != r * c) {
            return mat;
        }
        int[][] res = new int[r][c];
        for (int i = 0; i < row * column; i++) {
            res[i / c][i % c] = mat[i / column][i % column];
        }
        return res;
    }
}
