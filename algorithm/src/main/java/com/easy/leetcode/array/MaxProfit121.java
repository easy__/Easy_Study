package com.easy.leetcode.array;

/**
 * @ClassName MaxProfit121
 * @Description 121. 买卖股票的最佳时机
 * @Author zheng
 * @Date 2021/9/6 16:13
 * @Version 1.0
 **/
public class MaxProfit121 {
    public static int maxProfit(int[] prices) {
        int[] minArr = new int[prices.length];
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < prices.length; i++) {
            if (i - 1 >= 0) {
                minArr[i] = min = Math.min(prices[i - 1], min);
            } else {
                minArr[i] = prices[0];
            }
        }
        int max = 0;
        for (int i = 1; i < prices.length; i++) {
            max = Math.max(max, prices[i] - minArr[i]);
        }
        return max;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{
                7, 1, 5, 3, 6, 4
        };
        System.out.println(maxProfit(arr));
    }
}
