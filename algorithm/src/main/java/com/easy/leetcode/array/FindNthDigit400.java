package com.easy.leetcode.array;

/**
 * @ClassName FindNthDigit400
 * @Description 400. 第 N 位数字
 * @Author zheng
 * @Date 2021/11/30 14:49
 * @Version 1.0
 **/
public class FindNthDigit400 {
    public int findNthDigit(int n) {
        int num = 1;
        int len = 1;
        while (n > 9 * (long) num * len) {
            n -= 9 * num * len;
            num *= 10;
            len++;
        }
        // n表示 剩余的位数， len 表示每一个的长度，num 表示该长度的第一个数
        // 确定是具体哪个整数
        num += (n - 1) / len;
        // 确定是这个整数中的哪个位
        n -= (n - 1) / len * len;

        // 取出那个位的数字
        return (num / (int) Math.pow(10, len - n)) % 10;

    }


    public static void main(String[] args) {
        FindNthDigit400 findNthDigit400 = new FindNthDigit400();
        System.out.println(findNthDigit400.findNthDigit(1000000000));
    }
}
