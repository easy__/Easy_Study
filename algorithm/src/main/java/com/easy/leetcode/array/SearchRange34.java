package com.easy.leetcode.array;

import java.util.Arrays;

/**
 * @ClassName SearchRange34
 * @Description 34. 在排序数组中查找元素的第一个和最后一个位置
 * @Author zheng
 * @Date 2021/11/15 16:41
 * @Version 1.0
 **/
public class SearchRange34 {
    public int[] searchRange(int[] nums, int target) {
        if (nums.length == 0) {
            return new int[]{-1, -1};
        }
        int left = binarySearch(nums, target);
        int right = binarySearch(nums, target + 1);
        if (left == nums.length || nums[left] != target) {
            return new int[]{-1, -1};
        }
        return new int[]{left, right - 1};
    }


    int binarySearch(int[] nums, int target) {
        int left = 0;
        int right = nums.length;
        while (left < right) {
            int mid = (left + right) / 2;
            mid = left + (right - left) / 2;
            if (nums[mid] < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }

    public static void main(String[] args) {
        SearchRange34 searchRange34 = new SearchRange34();
        System.out.println(Arrays.toString(searchRange34.searchRange(new int[]{5, 7, 7, 8, 8, 10}, 10)));
    }

}
