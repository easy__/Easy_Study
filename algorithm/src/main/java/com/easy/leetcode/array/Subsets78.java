package com.easy.leetcode.array;

import java.util.ArrayList;
import java.util.List;
//8. 子集
public class Subsets78 {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        backTracking(res,path,nums,0);
        return res;
    }
    public void backTracking(List<List<Integer>> res,List<Integer> path,int[] nums,int start){
        //退出条件
        res.add(new ArrayList<>(path));
        for (int i = start; i < nums.length; i++) {
            //处理节点
            path.add(nums[i]);
            //递归函数
            backTracking(res,path,nums,i+1);
            //撤销操作
            path.remove(path.size()-1);
        }
    }

    public static void main(String[] args) {
        Subsets78 subsets78 = new Subsets78();
        subsets78.subsets(new int[]{ 1,2,3});
    }
}
