package com.easy.leetcode.array;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName Convert6
 * @Description 6. Z 字形变换
 * @Author zheng
 * @Date 2021/12/3 17:21
 * @Version 1.0
 **/
public class Convert6 {
    public String convert(String s, int numRows) {
        if (numRows == 1) {
            return s;
        }
        List<List<Character>> res = new ArrayList<>();
        for (int i = 0; i < numRows; i++) {
            res.add(new ArrayList<Character>());
        }
        int i = 0;
        int len = s.length();
        int index = 0;
        boolean flag = false;
        while (index < len) {
            if (!flag) {
                if (index == 0) {
                    res.get(i).add(s.charAt(index));
                } else {
                    res.get(++i).add(s.charAt(index));
                }
                if (i == numRows - 1) {
                    flag = true;
                }
            } else {
                res.get(--i).add(s.charAt(index));
                if (i == 0) {
                    flag = false;
                }
            }
            index++;
        }
        StringBuilder sb = new StringBuilder();
        for (List<Character> re : res) {
            String str = re.stream().map(Object::toString).collect(Collectors.joining());
            sb.append(str);
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

    public static void main(String[] args) {
        Convert6 convert6 = new Convert6();
        convert6.convert("PAYPALISHIRING",4);
    }
}
