package com.easy.leetcode.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName ThreeSum15
 * @Description 15. 三数之和
 * @Author zheng
 * @Date 2021/10/20 20:51
 * @Version 1.0
 **/
public class ThreeSum15 {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(nums);
        //数组长度小于3，不存在这样的数
        //排序后的第一个值大于0，不存在这样的三元组
        if (nums.length < 3 || nums[0] > 0) {
            return res;
        }
        for (int i = 0; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            int l = i + 1;
            int r = nums.length - 1;
            while (l < r) {
                //记录当前值
                if (nums[i] + nums[l] + nums[r] == 0) {
                    List<Integer> lst = new ArrayList<>();
                    lst.add(nums[i]);
                    lst.add(nums[l]);
                    lst.add(nums[r]);
                    res.add(lst);
                    //跳过重复值
                    while (l < r && nums[l] == nums[l + 1]) {
                        l++;
                    }
                    while (l < r && nums[r] == nums[l - 1]) {
                        r--;
                    }
                    l++;
                    r--;
                } else if (nums[i] + nums[l] + nums[r] > 0) {
                    //和大于0，说明正数太大，缩小
                    r--;
                } else {
                    l++;
                }
            }
        }
        return res;
    }
}
