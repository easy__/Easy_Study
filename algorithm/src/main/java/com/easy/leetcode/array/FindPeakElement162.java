package com.easy.leetcode.array;

/**
 * 寻找峰值
 */
public class FindPeakElement162 {
    //nums[-1] = nums[n] = -∞ 。
    //题意，最大值两边一定有值，那么最大值一定是峰值
    public int findPeakElement(int[] nums) {
        int max=0;
        for (int i = 1; i <nums.length ; i++) {
            if (nums[i]>nums[max]){
                max=i;
            }
        }
        return max;
    }

    public int findPeakElement1(int[] nums) {
        int left=0;
        int right=nums.length-1;
        while (left<right){
            int mid =left+(right-left)/2;
            //[mid+1,nums.length-1]到最后，一定有峰值
            if (nums[mid]<nums[mid+1]){
                left=mid+1;
            }else {
                //mid 之前有峰
                right=mid;
            }
        }
        //退出条件，left=right
        return right;
    }
}
