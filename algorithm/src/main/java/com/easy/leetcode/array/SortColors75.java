package com.easy.leetcode.array;

/**
 * @ClassName SortColors75
 * @Description 75. 颜色分类
 * @Author zheng
 * @Date 2021/12/7 20:06
 * @Version 1.0
 **/
public class SortColors75 {
    public void sortColors(int[] nums) {
        //双指针
        int start = 0;
        int end = nums.length - 1;
        for (int i = 0; i <= end; i++) {
            //等于2，交换两个值的位置，但存在2 与2交换的情况，所以需要一直交换
            while (i <= end && nums[i] == 2) {
                int tmp = nums[end];
                nums[end] = nums[i];
                nums[i] = tmp;
                end--;
            }
            //等于0，交换索引位置的值与start 索引上的值
            if (nums[i] == 0) {
                int tmp = nums[start];
                nums[start] = nums[i];
                nums[i] = tmp;
                start++;
            }
        }
    }
}
