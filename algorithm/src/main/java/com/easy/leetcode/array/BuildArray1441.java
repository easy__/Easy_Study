package com.easy.leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName BuildArray1441
 * @Description 1441. 用栈操作构建数组
 * @Author zheng
 * @Date 2021/9/8 14:31
 * @Version 1.0
 **/
public class BuildArray1441 {
    public List<String> buildArray(int[] target, int n) {
        int i = 0;
        int j = 1;
        List<String> oper = new ArrayList<>();
        while (i < target.length && j <= n) {
            oper.add("Push");
            if (target[i] == j) {
                i++;
            } else {
                oper.add("Pop");
            }
            j++;
        }
        return oper;
    }
}
