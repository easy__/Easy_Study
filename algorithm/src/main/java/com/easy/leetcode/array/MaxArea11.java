package com.easy.leetcode.array;

/**
 * 11. 盛最多水的容器
 *
 * 双指针，从两头往中间移，移动高度小的指针，因为高度小的决定了高度的最大值，
 * 如果移动大的，长变小，面积的最大值不可能大过上一次的面积
 */
public class MaxArea11 {
    public int maxArea(int[] height) {
        int len = height.length;
        int i=0;
        int j=len-1;
        int maxArea=0;
        while (i<j){
            maxArea =Math.max(maxArea,Math.min(height[i],height[j])*(j-i));
            if (height[i]<height[j]){
                i++;
            }else {
                j--;
            }
        }
        return maxArea;
    }
}
