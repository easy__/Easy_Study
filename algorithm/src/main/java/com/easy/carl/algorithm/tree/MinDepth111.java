package com.easy.carl.algorithm.tree;

import com.easy.leetcode.tree.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @ClassName MinDepth111
 * @Description 111. 二叉树的最小深度
 * @Author zheng
 * @Date 2022/1/27 11:09
 * @Version 1.0
 **/
public class MinDepth111 {
    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        } else if (root.left == null) {
            return minDepth(root.right) + 1;
        } else if (root.right == null) {
            return minDepth(root.left) + 1;
        } else {
            return Math.min(minDepth(root.left), minDepth(root.right)) + 1;
        }
    }
}
