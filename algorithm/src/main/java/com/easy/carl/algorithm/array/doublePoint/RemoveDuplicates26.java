package com.easy.carl.algorithm.array.doublePoint;

/**
 * @ClassName RemoveDuplicates26
 * @Description TODO
 * @Author zheng
 * @Date 2021/12/10 16:24
 * @Version 1.0
 **/
public class RemoveDuplicates26 {
    public int removeDuplicates(int[] nums) {
        if (nums.length < 2) {
            return nums.length;
        }
        int slow = 0;
        for (int quick = 0; quick < nums.length; quick++) {
            if (nums[slow] != nums[quick]) {
                slow++;
                nums[slow] = nums[quick];
            }
        }
        return slow + 1;
    }

    public static void main(String[] args) {
        RemoveDuplicates26 removeDuplicates26 = new RemoveDuplicates26();
        System.out.println(removeDuplicates26.removeDuplicates(new int[]{0,0,1,1,1,2,2,3,3,4}));
    }
}
