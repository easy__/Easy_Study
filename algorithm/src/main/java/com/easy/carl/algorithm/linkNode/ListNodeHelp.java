package com.easy.carl.algorithm.linkNode;

import com.easy.leetcode.ListNode;

/**
 * 辅助类
 */
public class ListNodeHelp {
    public static ListNode helpBuild(int[] nums) {
        ListNode head = new ListNode(-1);
        ListNode tmp = head;
        for (int num : nums) {
            tmp.next = new ListNode(num);
            tmp = tmp.next;
        }
        return head.next;
    }

    public static void helpPrint(ListNode listNode) {
        StringBuffer sb = new StringBuffer();
        while (listNode != null) {
            sb.append(listNode.val).append("->");
            listNode=listNode.next;
        }
        System.out.println(sb.toString());
    }
}
