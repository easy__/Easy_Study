package com.easy.carl.algorithm.StackAndQueue;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @ClassName MyStack225
 * @Description 225. 用队列实现栈
 * @Author zheng
 * @Date 2021/12/29 16:57
 * @Version 1.0
 **/
public class MyStack225 {
    Deque<Integer> queue1;
    Deque<Integer> queue2;

    public MyStack225() {
        queue1 = new LinkedList<>();
        queue2 = new LinkedList<>();
    }

    public void push(int x) {
        if (queue1.isEmpty()) {
            queue1.add(x);
            while (!queue2.isEmpty()) {
                queue1.add(queue2.pollFirst());
            }
        } else {
            queue2.add(x);
            while (!queue1.isEmpty()) {
                queue2.add(queue1.pollFirst());
            }
        }
    }

    public int pop() {
        return queue1.isEmpty() ? queue2.pollFirst() : queue1.pollFirst();
    }

    public int top() {
        return queue1.isEmpty() ? queue2.peekFirst() : queue1.peekFirst();
    }

    public boolean empty() {
        return queue1.isEmpty() && queue2.isEmpty();
    }
}
