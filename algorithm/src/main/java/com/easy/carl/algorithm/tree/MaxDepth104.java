package com.easy.carl.algorithm.tree;

import com.easy.leetcode.tree.TreeNode;

/**
 * @ClassName MaxDepth104
 * @Description 104. 二叉树的最大深度
 * @Author zheng
 * @Date 2022/1/27 10:56
 * @Version 1.0
 **/
public class MaxDepth104 {
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(maxDepth(root.left) + 1, maxDepth(root.right) + 1);
    }

}
