package com.easy.carl.algorithm.hashMap;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName FourSumCount454
 * @Description 454. 四数相加 II
 * @Author zheng
 * @Date 2021/12/21 10:27
 * @Version 1.0
 **/
public class FourSumCount454 {
    public int fourSumCount(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {
        Map<Integer, Integer> map = new HashMap<>(16);
        //结果
        int res = 0;
        //两个一组，求所有可能的和，并记录
        for (int k1 : nums1) {
            for (int k2 : nums2) {
                int sum = k1 + k2;
                map.put(sum, map.getOrDefault(sum, 0) + 1);
            }
        }
        //两个一组，求所有可能的和,
        for (int k3 : nums3) {
            for (int k4 : nums4) {
                int sum = k3 + k4;
                //在map中查找是否存在等于0的数
                if (map.getOrDefault(-sum, 0) > 0) {
                    res+=map.get(-sum);
                }
            }
        }
        System.out.println(res);
        return res;
    }

    public static void main(String[] args) {
        FourSumCount454 fourSumCount454 = new FourSumCount454();
        fourSumCount454.fourSumCount(new int[]{-1, -1}, new int[]{-1, 1}, new int[]{-1, 1}, new int[]{1, -1});
    }
}
