package com.easy.carl.algorithm.array.doublePoint;

import java.util.Arrays;

public class SortedSquares977 {
    public int[] sortedSquares(int[] nums) {
        //左指针
        int left=0;
        //右指针
        int right=nums.length-1;
        //结果
        int[] res=new int[nums.length];
        //结果指针
        int r=nums.length-1;
        while (left<=right){
            //左边的绝对值大于右边的绝对值
            if (Math.abs(nums[left])>Math.abs(nums[right])){
                //取左边的平方，放入最后一个位置
                res[r]=nums[left]*nums[left];
                //左指针右移
                left++;
            }else {
                //右边的绝对值大于左边的绝对值
                //取右边的平方放入结果中
                res[r]=nums[right]*nums[right];
                //右指针左移
                right--;
            }
            //结果指针左移
            r--;
        }
        System.out.println(Arrays.toString(res));
        return res;
    }

    public static void main(String[] args) {
        SortedSquares977 sortedSquares977 = new SortedSquares977();
        System.out.println(sortedSquares977.sortedSquares(new int[]{-4,-1,0,3,10}).toString());
    }
}
