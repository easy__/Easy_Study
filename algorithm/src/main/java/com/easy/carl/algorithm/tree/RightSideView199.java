package com.easy.carl.algorithm.tree;

import com.easy.leetcode.tree.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * @ClassName RightSideView199
 * @Description 二叉树的右视图
 * @Author zheng
 * @Date 2022/1/26 16:42
 * @Version 1.0
 **/
public class RightSideView199 {
    public List<Integer> rightSideView(TreeNode root) {
        Queue<TreeNode> queue = new ArrayDeque<>();
        //结果
        List<Integer> res = new ArrayList<>();
        if (root != null) {
            queue.offer(root);
        }
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode pollNode = queue.poll();
                //当前层最右侧的节点才存放进来
                if (i == 0) {
                    res.add(pollNode.val);
                }
                if (pollNode.right != null) {
                    queue.offer(pollNode.right);
                }
                if (pollNode.left != null) {
                    queue.offer(pollNode.left);
                }
            }
        }
        return res;
    }
}
