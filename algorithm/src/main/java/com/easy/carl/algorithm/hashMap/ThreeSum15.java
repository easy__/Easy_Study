package com.easy.carl.algorithm.hashMap;

import java.util.*;

/**
 * @ClassName ThreeSum15
 * @Description 15. 三数之和
 * @Author zheng
 * @Date 2021/12/21 15:45
 * @Version 1.0
 **/
public class ThreeSum15 {
    /**
     * @return java.util.List<java.util.List < java.lang.Integer>>
     * @Description map 法，效率低
     * @Date 2021/12/21 17:28
     * @Param [nums]
     **/
    public List<List<Integer>> threeSum1(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        //排序，所有相同的排一起
        Arrays.sort(nums);
        if (nums.length < 3 || nums[0] > 0) {
            return res;
        }
        Map<Integer, Integer> map = new HashMap();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], map.get(nums[i]) - 1);
            //剪枝，i>0时，且当前值与前一个值相同，则可能出现同样的数据，跳过
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            Map<Integer, Integer> tempMap = new HashMap<>(map);
            for (int j = i + 1; j < nums.length; j++) {
                tempMap.put(nums[j], tempMap.get(nums[j]) - 1);
                //剪枝，j > i + 1时，且当前值与前一个值相同，则可能出现同样的数据，跳过
                if (j > i + 1 && nums[j] == nums[j - 1]) {
                    continue;
                }
                int sum = -nums[i] - nums[j];
                if (tempMap.getOrDefault(sum, 0) > 0) {
                    res.add(Arrays.asList(nums[i], nums[j], sum));
                }
            }
        }
        return res;
    }

    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        //排序，方便去重
        Arrays.sort(nums);
        //三数之和，小于三或者第一个数大于0 ，一定不符合条件
        if (nums.length < 3 || nums[0] > 0) {
            return res;
        }
        for (int i = 0; i < nums.length; i++) {
            //去重
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            //左指针
            int L = i + 1;
            //右指针
            int R = nums.length - 1;
            while (L < R) {
                //符合条件，记录当前值
                if (nums[i] + nums[L] + nums[R] == 0) {
                    res.add(Arrays.asList(nums[i], nums[L], nums[R]));
                    //下一个值如果与当前值一直，去重 R 是最后一个索引，L<R可以防止指针越界
                    while (L < R && nums[L] == nums[L + 1]) {
                        L++;
                    }
                    while (L < R && nums[R] == nums[R - 1]) {
                        R--;
                    }
                    L++;
                    R--;
                } else if (nums[i] + nums[L] + nums[R] > 0) {
                    //和大于0，收缩大的值，即右边界
                    R--;
                } else {
                    //和小于0，收缩左边界，
                    L++;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        ThreeSum15 threeSum15 = new ThreeSum15();
        System.out.println(threeSum15.threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
    }
}
