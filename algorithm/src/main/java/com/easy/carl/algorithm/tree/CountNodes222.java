package com.easy.carl.algorithm.tree;

import com.easy.leetcode.tree.TreeNode;

import java.util.ArrayDeque;
import java.util.Queue;

public class CountNodes222 {
    public int countNodes(TreeNode root) {
        if (root==null){
            return 0;
        }
        int count=0;
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int size = queue.size();
            count+=size;
            while (size>=0){
                TreeNode poll = queue.poll();
                if (poll.left!=null) {
                    queue.offer(poll.left);
                }
                if (poll.right!=null) {
                    queue.offer(poll.right);
                }
                size--;
            }
        }
        return count;
    }

    public int countNodes1(TreeNode root){
        if (root==null){
            return 0;
        }
        return countNodes1(root.left)+countNodes1(root.right)+1;
    }
}
