package com.easy.carl.algorithm.tree;

import com.easy.leetcode.tree.TreeNode;

/**
 * @ClassName InvertTree226
 * @Description 226. 翻转二叉树
 * @Author zheng
 * @Date 2022/1/20 20:25
 * @Version 1.0
 **/
public class InvertTree226 {
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode tmp = root.left;
        root.left = root.right;
        root.right = tmp;
        invertTree(root.left);
        invertTree(root.right);
        return root;
    }
}
