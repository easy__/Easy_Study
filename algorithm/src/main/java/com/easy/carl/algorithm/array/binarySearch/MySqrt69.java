package com.easy.carl.algorithm.array.binarySearch;

/**
 * @ClassName MySqrt69
 * @Description 69. Sqrt(x)
 * @Author zheng
 * @Date 2021/12/10 9:26
 * @Version 1.0
 **/
public class MySqrt69 {
    public int mySqrt1(int x) {
        Long sum = 0L;
        while (sum * sum <= x) {
            sum++;
        }
        return sum.intValue() - 1;
    }

    public int mySqrt(int x) {
        long left = 0L;
        long right = x;
        while (left <= right) {
            Long mid = left + (right - left) / 2;
            if (mid * mid < x) {
                left = mid + 1;
            } else if (mid * mid == x) {
                return mid.intValue();
            } else {
                right = mid - 1;
            }
        }
        //System.out.println(left);
        //System.out.println(right);
        return (int) right;
    }


    public static void main(String[] args) {
        MySqrt69 mySqrt69 = new MySqrt69();
        System.out.println(mySqrt69.mySqrt(2147395599));
    }
}
