package com.easy.carl.algorithm.hashMap;

import java.util.*;

/**
 * @ClassName FourSum18
 * @Description 18. 四数之和
 * @Author zheng
 * @Date 2021/12/21 18:01
 * @Version 1.0
 **/
public class FourSum18 {
    public List<List<Integer>> fourSum1(int[] nums, int target) {
        List<List<Integer>> res = new ArrayList<>();
        //排序，所有相同的排一起
        Arrays.sort(nums);
        if (nums.length < 4) {
            return res;
        }
        Map<Integer, Integer> map = new HashMap();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], map.get(nums[i]) - 1);
            //剪枝，i>0时，且当前值与前一个值相同，则可能出现同样的数据，跳过
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            Map<Integer, Integer> tempMap = new HashMap<>(map);
            for (int j = i + 1; j < nums.length; j++) {
                tempMap.put(nums[j], tempMap.get(nums[j]) - 1);
                //剪枝，j > i + 1时，且当前值与前一个值相同，则可能出现同样的数据，跳过
                if (j > i + 1 && nums[j] == nums[j - 1]) {
                    continue;
                }
                Map<Integer, Integer> tempMap1 = new HashMap<>(tempMap);
                for (int k = j + 1; k < nums.length; k++) {
                    tempMap1.put(nums[k], tempMap1.get(nums[k]) - 1);
                    ///剪枝，k > j + 1时，且当前值与前一个值相同，则可能出现同样的数据，跳过
                    if (k > j + 1 && nums[k] == nums[k - 1]) {
                        continue;
                    }
                    int sum = target - nums[i] - nums[j] - nums[k];
                    if (tempMap1.getOrDefault(sum, 0) > 0) {
                        res.add(Arrays.asList(nums[i], nums[j], nums[k], sum));
                    }
                }

            }
        }
        return res;
    }

    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> res = new ArrayList<>();
        //排序，所有相同的排一起
        Arrays.sort(nums);
        if (nums.length < 4 || nums[0] > target) {
            return res;
        }
        for (int i = 0; i < nums.length - 3; i++) {
            //去重
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            for (int j = i + 1; j < nums.length - 2; j++) {
                if (j > i + 1 && nums[j] == nums[j - 1]) {
                    continue;
                }
                //左指针
                int L = j + 1;
                //右指针
                int R = nums.length - 1;
                while (L < R) {
                    //符合条件，记录当前值
                    if (nums[i] + nums[j] + nums[L] + nums[R] == target) {
                        res.add(Arrays.asList(nums[i], nums[j], nums[L], nums[R]));
                        //下一个值如果与当前值一直，去重 R 是最后一个索引，L<R可以防止指针越界
                        while (L < R && nums[L] == nums[L + 1]) {
                            L++;
                        }
                        while (L < R && nums[R] == nums[R - 1]) {
                            R--;
                        }
                        L++;
                        R--;
                    } else if (nums[i] + nums[j] + nums[L] + nums[R] > target) {
                        //和大于0，收缩大的值，即右边界
                        R--;
                    } else {
                        //和小于0，收缩左边界，
                        L++;
                    }
                }
            }

        }
        return res;
    }

    public static void main(String[] args) {
        FourSum18 fourSum18 = new FourSum18();
        System.out.println(fourSum18.fourSum(new int[]{1,-2,-5,-4,-3,3,3,5}, -11));
    }
}
