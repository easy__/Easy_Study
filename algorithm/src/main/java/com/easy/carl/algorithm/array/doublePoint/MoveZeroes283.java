package com.easy.carl.algorithm.array.doublePoint;

import java.util.Arrays;

/**
 * @ClassName MoveZeroes283
 * @Description TODO
 * @Author zheng
 * @Date 2021/12/10 16:37
 * @Version 1.0
 **/
public class MoveZeroes283 {
    public void moveZeroes(int[] nums) {
        int slow = 0;
        for (int quick = 0; quick < nums.length; quick++) {
            if (nums[quick] != 0) {
                nums[slow] = nums[quick];
                slow++;
            }
        }
        while (slow < nums.length) {
            nums[slow] = 0;
            slow++;
        }
        System.out.println(Arrays.toString(nums));
    }

    public void moveZeroes1(int[] nums) {
        int slow = 0;
        int quick = 0;
        while (quick < nums.length) {
            if (nums[quick] != 0) {
                //交换位置
                int tmp = nums[slow];
                nums[slow] = nums[quick];
                nums[quick] = tmp;
                slow++;
            }
            quick++;
        }
        //System.out.println(Arrays.toString(nums));
    }

    public static void main(String[] args) {
        MoveZeroes283 moveZeroes283 = new MoveZeroes283();
        moveZeroes283.moveZeroes1(new int[]{0, 1, 0, 3, 12});
    }
}
