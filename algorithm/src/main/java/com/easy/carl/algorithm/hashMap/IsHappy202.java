package com.easy.carl.algorithm.hashMap;

import java.util.HashSet;
import java.util.Set;

/**
 * @ClassName IsHappy
 * @Description 202. 快乐数
 * @Author zheng
 * @Date 2021/12/20 17:31
 * @Version 1.0
 **/
public class IsHappy202 {
    public boolean isHappy(int n) {
        Set<Integer> set = new HashSet<>();
        //n 不等于1 ，一直执行
        while (n != 1) {
            //和
            int sum = 0;
            //每个位置的平方 和
            while (n > 0) {
                //余数
                int a = n % 10;
                sum += a * a;
                n = n / 10;
            }
            //set 中包含，则说明进入了死循环，返回false
            if (set.contains(sum)) {
                return false;
            }
            set.add(sum);
            //重置n
            n = sum;
        }
        return true;
    }

    public static void main(String[] args) {
        IsHappy202 isHappy202 = new IsHappy202();
        System.out.println(isHappy202.isHappy(19));
    }
}
