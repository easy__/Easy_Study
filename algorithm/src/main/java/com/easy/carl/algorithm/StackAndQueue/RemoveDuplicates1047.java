package com.easy.carl.algorithm.StackAndQueue;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

/**
 * @ClassName RemoveDuplicates1047
 * @Description 1047. 删除字符串中的所有相邻重复项
 * @Author zheng
 * @Date 2021/12/30 10:00
 * @Version 1.0
 **/
public class RemoveDuplicates1047 {
    public String removeDuplicates(String s) {
        Deque<Character> deque = new ArrayDeque<>();
        char[] chars = s.toCharArray();
        for (char c : chars) {
            if (!deque.isEmpty() && deque.peekLast() == c) {
                deque.pollLast();
            } else {
                deque.addLast(c);
            }
        }
        StringBuilder sb = new StringBuilder();
        while (!deque.isEmpty()) {
            sb.append(deque.pollFirst());
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        RemoveDuplicates1047 removeDuplicates1047 = new RemoveDuplicates1047();
        System.out.println(removeDuplicates1047.removeDuplicates("abbaca"));
    }
}
