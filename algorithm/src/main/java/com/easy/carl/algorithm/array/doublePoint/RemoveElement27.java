package com.easy.carl.algorithm.array.doublePoint;

/**
 * @ClassName RemoveElement27
 * @Description 27. 移除元素
 * @Author zheng
 * @Date 2021/12/10 15:32
 * @Version 1.0
 **/
public class RemoveElement27 {
    public int removeElement(int[] nums, int val) {
        int n = nums.length;
        int left = 0;
        for (int i = 0; i < n; i++) {
            if (nums[i] != val) {
                nums[left] = nums[i];
                left++;
            }
        }
        return left;
    }


    public static void main(String[] args) {
        RemoveElement27 removeElement27 = new RemoveElement27();
        System.out.println(removeElement27.removeElement(new int[]{1}, 1));
    }
}
