package com.easy.carl.algorithm.linkNode;

import com.easy.leetcode.ListNode;

/**
 * @ClassName ReverseList206
 * @Description 206. 反转链表
 * @Author zheng
 * @Date 2021/12/17 16:01
 * @Version 1.0
 **/
public class ReverseList206 {
    public ListNode reverseList(ListNode head) {
        ListNode pre = null;
        while (head != null) {
            //保存当前节点
            ListNode tmp = head;
            //当前指针后移
            head = head.next;
            //更新指针
            tmp.next = pre;
            //更新pre 节点
            pre = tmp;
        }
        return pre;
    }

    public static void main(String[] args) {
        ReverseList206 reverseList206 = new ReverseList206();
        ListNode listNode = reverseList206.reverseList(reverseList206.help(new int[]{1, 2, 3, 4, 5, 6}));
        System.out.println(listNode);
    }

    ListNode help(int[] nums) {
        ListNode head = new ListNode(-1);
        ListNode tmp = head;
        for (int num : nums) {
            tmp.next = new ListNode(num);
            tmp = tmp.next;
        }
        return head.next;
    }
}
