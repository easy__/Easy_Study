package com.easy.carl.algorithm.tree;

import java.util.*;

public class LevelOrder429 {
    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> res = new ArrayList();
        Queue<Node> queue = new ArrayDeque<>();
        if (root!=null){
            queue.offer(root);
        }
        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> levelList = new ArrayList<>();
            for (int i = 0 ; i <size ; i++) {
                Node node = queue.poll();
                levelList.add(node.val);
                if (node.children!=null) {
                    List<Node> children = node.children;
                    for (Node child : children) {
                        queue.offer(child);
                    }
                }
            }
            res.add(levelList);
        }
        return res;
    }
}
