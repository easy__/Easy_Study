package com.easy.carl.algorithm.String;

import java.util.Arrays;

/**
 * @ClassName reverseString
 * @Description 344. 反转字符串
 * @Author zheng
 * @Date 2021/12/24 15:35
 * @Version 1.0
 **/
public class ReverseString344 {
    /**
     * @return void
     * @Description 344. 反转字符串
     * @Date 2021/12/24 15:36
     * @Param [s]
     **/
    public void reverseString(char[] s) {
        int len = s.length;
        for (int i = 0; i < len; i++) {
            char tmp = s[i];
            s[i] = s[len - 1];
            s[len - 1] = tmp;
            len--;
        }
    }

    public static void main(String[] args) {
        ReverseString344 reverseString344 = new ReverseString344();
        char[] s = new char[]{'H', 'a', 'n', 'n', 'a', 'h'};
        reverseString344.reverseString(s);
        System.out.println(Arrays.toString(s));
    }
}
