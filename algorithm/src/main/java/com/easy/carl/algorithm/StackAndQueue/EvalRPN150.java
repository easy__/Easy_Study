package com.easy.carl.algorithm.StackAndQueue;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @ClassName EvalRPN150
 * @Description 150. 逆波兰表达式求值
 * @Author zheng
 * @Date 2021/12/30 10:38
 * @Version 1.0
 **/
public class EvalRPN150 {
    public int evalRPN(String[] tokens) {
        Deque<Integer> deque = new ArrayDeque<>();
        for (String token : tokens) {
            if ("+".equals(token)) {
                int a = deque.pop();
                int b = deque.pop();
                deque.push(b + a);
            } else if ("-".equals(token)) {
                int a = deque.pop();
                int b = deque.pop();
                deque.push(b - a);
            } else if ("*".equals(token)) {
                int a = deque.pop();
                int b = deque.pop();
                deque.push(b * a);
            } else if ("/".equals(token)) {
                int a = deque.pop();
                int b = deque.pop();
                deque.push(b / a);
            } else {
                deque.push(Integer.parseInt(token));
            }
        }
        return deque.pop();
    }

    public static void main(String[] args) {
        System.out.println(new EvalRPN150().evalRPN(new String[]{"10","6","9","3","+","-11","*","/","*","17","+","5","+"}));
    }
}
