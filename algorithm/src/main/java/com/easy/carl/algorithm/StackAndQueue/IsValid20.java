package com.easy.carl.algorithm.StackAndQueue;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @ClassName IsValid20
 * @Description 20. 有效的括号
 * @Author zheng
 * @Date 2021/12/29 17:26
 * @Version 1.0
 **/
public class IsValid20 {
    public boolean isValid(String s) {
        char[] chars = s.toCharArray();
        Deque<Character> deque = new ArrayDeque<>();
        //字符串长度不是2的倍数，肯定不满足条件
        if (chars.length % 2 != 0) {
            return false;
        }
        for (char c : chars) {
            if (c == '(') {
                deque.push(')');
            } else if (c == '[') {
                deque.push(']');
            } else if (c == '{') {
                deque.push('}');
            } else {
                if (deque.isEmpty() || deque.pop() != c) {
                    return false;
                }
            }
        }
        return deque.isEmpty();
    }

    public static void main(String[] args) {
        IsValid20 valid20 = new IsValid20();
        System.out.println(valid20.isValid("([)]"));
    }
}
