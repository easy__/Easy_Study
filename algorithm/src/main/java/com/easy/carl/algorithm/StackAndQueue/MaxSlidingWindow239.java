package com.easy.carl.algorithm.StackAndQueue;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

/**
 * @ClassName MaxSlidingWindow239
 * @Description 239. 滑动窗口最大值
 * @Author zheng
 * @Date 2021/12/30 11:17
 * @Version 1.0
 **/
public class MaxSlidingWindow239 {
    public int[] maxSlidingWindow1(int[] nums, int k) {
        int end = k - 1;
        int start = 0;
        while (end < nums.length) {
            int max = Integer.MIN_VALUE;
            for (int i = start; i <= end; i++) {
                max = Math.max(max, nums[i]);
            }
            nums[start] = max;
            start++;
            end++;
        }
        return Arrays.copyOf(nums, start);
    }

    public int[] maxSlidingWindow(int[] nums, int k) {
        //存放单调递减队列的索引
        Deque<Integer> deque = new ArrayDeque<>();
        //结果数组
        int[] res = new int[nums.length - k + 1];
        //遍历数组
        for (int i = 0; i < nums.length; i++) {
            //保证队列单调递减
            while (!deque.isEmpty() && nums[deque.peekLast()] <= nums[i]) {
                deque.pollLast();
            }
            deque.addLast(i);
            //队首的索引值+k，如果小于等于i,说明该索引滑出窗口外，直接移除
            if (deque.peekFirst() + k <= i) {
                deque.pollFirst();
            }
            //窗口内数据长度大于等于k时，才能进行计算。
            if (i + 1 >= k) {
                res[i + 1 - k] = nums[deque.peekFirst()];
            }

        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(new MaxSlidingWindow239().maxSlidingWindow(new int[]{4, -2}, 2)));
    }
}