package com.easy.carl.algorithm.hashMap;

import java.util.*;

/**
 * @ClassName Intersection349
 * @Description 349. 两个数组的交集
 * @Author zheng
 * @Date 2021/12/20 16:41
 * @Version 1.0
 **/
public class Intersection349 {
    /**
     * @return int[]
     * @Description 数组方式
     * @Date 2021/12/20 17:15
     * @Param [nums1, nums2]
     **/
    public int[] intersection1(int[] nums1, int[] nums2) {
        //结果
        List<Integer> res = new ArrayList<>();
        //空数组，直接返回
        if (nums1 == null || nums1.length == 0 || nums2 == null || nums2.length == 0) {
            return new int[0];
        }
        //排序，方便去重
        Arrays.sort(nums1);
        //Arrays.sort(nums2);
        for (int i = 0; i < nums1.length; i++) {
            //剪枝，当前值与前一个一样，直接跳过
            if (i > 0 && nums1[i] == nums1[i - 1]) {
                continue;
            }
            for (int j = 0; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    res.add(nums2[j]);
                    break;
                }

            }
        }
        return res.stream().mapToInt(Integer::intValue).toArray();
    }

    public int[] intersection(int[] nums1, int[] nums2) {
        List<Integer> res = new ArrayList<>();
        Set<Integer> nums1Set = new HashSet();
        for (int i : nums1) {
            nums1Set.add(i);
        }
        for (int i : nums2) {
            if (nums1Set.contains(i)) {
                res.add(i);
                nums1Set.remove(i);
            }
        }
        return res.stream().mapToInt(Integer::intValue).toArray();
    }


    public static void main(String[] args) {
        Intersection349 intersection349 = new Intersection349();
        int[] res = intersection349.intersection(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4});
        System.out.println(Arrays.toString(res));
        ;
    }
}
