package com.easy.carl.algorithm.StackAndQueue;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/**
 * @ClassName MyQueue232
 * @Description 232. 用栈实现队列
 * @Author zheng
 * @Date 2021/12/29 16:28
 * @Version 1.0
 **/
public class MyQueue232 {

    Deque<Integer> in;
    Deque<Integer> out;

    public MyQueue232() {
        in = new LinkedList<>();
        out = new LinkedList<>();
    }

    public void push(int x) {
        in.push(x);
    }

    public int pop() {
        if (out.isEmpty()) {
            in2out();
        }
        return out.pop();
    }

    public int peek() {
        if (out.isEmpty()) {
            in2out();
        }
        return out.peek();
    }

    public boolean empty() {
        return in.isEmpty() && out.isEmpty();
    }

    private void in2out() {
        while (!in.isEmpty()) {
            out.push(in.pop());
        }
    }

    public static void main(String[] args) {
        MyQueue232 myQueue232 = new MyQueue232();
        myQueue232.push(1);
        myQueue232.push(2);
        System.out.println(myQueue232.peek());
        System.out.println(myQueue232.pop());
        System.out.println(myQueue232.empty());
    }
}
