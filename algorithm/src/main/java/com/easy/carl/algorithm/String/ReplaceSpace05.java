package com.easy.carl.algorithm.String;

/**
 * 剑指 Offer 05. 替换空格
 */
public class ReplaceSpace05 {
    public String replaceSpace1(String s) {
        char[] chars = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : chars) {
            if (c == ' ') {
                sb.append("%20");
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

}
