package com.easy.carl.algorithm.linkNode;

import com.easy.leetcode.ListNode;

import java.util.List;

/**
 * @ClassName RemoveElements203
 * @Description 203. 移除链表元素
 * @Author zheng
 * @Date 2021/12/17 10:07
 * @Version 1.0
 **/
public class RemoveElements203 {
    public ListNode removeElements(ListNode head, int val) {
        //临时节点
        ListNode tmp = new ListNode(-1, head);
        //前一个节点
        ListNode pre = tmp;
        //当前节点不为空
        while (head != null) {
            //当前节点的值与目标值一致
            if (head.val == val) {
                //前一个节点的next设置为当前节点的next
                pre.next = head.next;
            } else {
                //前一个节点设置为当前节点
                pre = head;
            }
            //当前节点后移
            head = head.next;
        }
        return tmp.next;
    }

    ListNode help(int[] nums) {
        ListNode head = new ListNode(-1);
        ListNode tmp = head;
        for (int num : nums) {
            tmp.next = new ListNode(num);
            tmp = tmp.next;
        }
        return head.next;
    }

    public static void main(String[] args) {
        RemoveElements203 removeElements203 = new RemoveElements203();
        ListNode res = removeElements203.removeElements(removeElements203.help(new int[]{1, 2, 6, 3, 4, 5, 6}), 6);
        System.out.println(res);
    }
}
