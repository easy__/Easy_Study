package com.easy.carl.algorithm.tree.tmp;

/**
 * @ClassName Node
 * @Description TODO
 * @Author zheng
 * @Date 2022/1/27 10:32
 * @Version 1.0
 **/
public class Node {
    public int val;
    public Node left;
    public Node right;
    public Node next;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, Node _left, Node _right, Node _next) {
        val = _val;
        left = _left;
        right = _right;
        next = _next;
    }
}
