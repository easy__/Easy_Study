package com.easy.carl.algorithm.array.binarySearch;

/**
 * @ClassName Search704
 * @Description 二分查找
 * @Author zheng
 * @Date 2021/12/9 15:27
 * @Version 1.0
 **/
public class Search704 {
    public int search(int[] nums, int target) {
        //左边界
        int left = 0;
        //右边界是nums.length,nums[right]其实是无效位置
        //所以整个区间是左闭右开区间
        int right = nums.length;
        //左闭右开区间
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] < target) {
                //nums[mid]的值小于target，下一次从右区间找
                //因为下一次比较时包含左边界，所以left的值要更新为mid+1;
                left = mid + 1;
            } else {
                //nums[mid]的值大于target，下一次去左区间找
                //因为是左闭右开区间，下一次不会比较right位置上的值
                //所以right更新为mid
                right = mid;

            }
        }
        System.out.println(left);
        System.out.println(right);
        return -1;
    }

    /**
     * @return int
     * @Description 左闭 右闭区间
     * @Date 2021/12/9 16:03
     * @Param [nums, target]
     **/
    public int search1(int[] nums, int target) {
        //左边界
        int left = 0;
        //右边界是nums.length-1,nums[right]位置有效
        //所以整个区间是左闭右闭区间
        int right = nums.length - 1;
        //左闭右闭
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] > target) {
                //nums[mid]的值大于target，下一次去左区间找
                //因为是左闭右闭区间，下一次会比较right位置上的值
                //所以right更新为mid-1
                right = mid - 1;
            } else {
                //nums[mid]的值小于target，下一次从右区间找
                //因为下一次比较时包含左边界，所以left的值要更新为mid+1;
                left = mid + 1;
            }
        }
        return -1;
    }


    public static void main(String[] args) {
        Search704 search704 = new Search704();
        System.out.println(search704.search(new int[]{
                1, 3, 5
        }, 1));
    }
}
