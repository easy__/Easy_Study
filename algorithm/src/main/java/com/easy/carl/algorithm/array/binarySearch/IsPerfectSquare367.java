package com.easy.carl.algorithm.array.binarySearch;

/**
 * @ClassName IsPerfectSquare367
 * @Description 367. 有效的完全平方数
 * @Author zheng
 * @Date 2021/12/10 15:19
 * @Version 1.0
 **/
public class IsPerfectSquare367 {
    public boolean isPerfectSquare(int num) {
        long left = 0;
        long right = num;
        while (left <= right) {
            long mid = left + (right - left) / 2;
            if (mid * mid < num) {
                left = mid + 1;
            } else if (mid * mid == num) {
                return true;
            } else {
                right = mid - 1;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        IsPerfectSquare367 isPerfectSquare367 = new IsPerfectSquare367();
        System.out.println(isPerfectSquare367.isPerfectSquare(2147483647));
    }
}
