package com.easy.carl.algorithm.linkNode;

import com.easy.leetcode.ListNode;

/**
 * 24. 两两交换链表中的节点
 */
public class SwapPairs24 {
    public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode pre = new ListNode(-1, head);
        ListNode tmp =pre;
        ListNode cur = head;
        //cur =1  cur.next=2; pre =-1
        while (cur!=null&& cur.next!=null){
            //记录当前节点后第二个节点为next
            ListNode next= cur.next.next;
            //前一个节点的next 指向当前节点的next
            pre.next=cur.next;
            //当前节点的下一个节点的next 指向当前节点
            cur.next.next=cur;
            //当前节点的next 指向当前节点后第二个节点
            cur.next=next;
            pre=cur;
            cur=next;
        }
        return tmp.next;
    }

    public static void main(String[] args) {
        ListNode listNode= ListNodeHelp.helpBuild(new int[]{1,2,3});
        SwapPairs24 swapPairs24 = new SwapPairs24();
        ListNodeHelp.helpPrint(swapPairs24.swapPairs(listNode));
    }
}
