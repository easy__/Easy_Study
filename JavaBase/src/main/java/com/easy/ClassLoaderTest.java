package com.easy;

import java.io.IOException;
import java.io.InputStream;

/**
 * @ClassName ClassLoaderTest
 * @Description TODO
 * @Author zheng
 * @Date 2022/7/8 15:01
 * @Version 1.0
 **/
public class ClassLoaderTest {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        ClassLoader classLoader = new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                String fileName= name.substring(name.lastIndexOf(".")+1)+".class";
                InputStream resourceAsStream = getClass().getResourceAsStream(fileName);
                if (resourceAsStream==null) {
                    return super.loadClass(name);
                }
                try {
                    byte[] bytes = new byte[resourceAsStream.available()];
                    resourceAsStream.read(bytes);
                    return defineClass(name,bytes,0,bytes.length);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        Object o = classLoader.loadClass("com.easy.ClassLoaderTest").newInstance();

        System.out.println(o.getClass());
        System.out.println(o instanceof ClassLoaderTest);
        System.out.println( o.getClass().getClassLoader());
        System.out.println( ClassLoaderTest.class.getClassLoader());
    }

}
