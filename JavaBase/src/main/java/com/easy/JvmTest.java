package com.easy;

/**
 * @ClassName JvmTest
 * @Description TODO
 * @Author zheng
 * @Date 2022/6/23 11:05
 * @Version 1.0
 **/
public class JvmTest {

    private static final int _1MB = 1024 * 1024;

    public static void main(String[] args) {
        byte[] allocation1, allocation2, allocation3;
        allocation1 = new byte[_1MB / 4];
        allocation2 = new byte[_1MB * 4];
        allocation3 = new byte[_1MB * 4];
        allocation3 = null;
        allocation3 = new byte[_1MB * 4];
    }

    public String a;
    public String[] arr;
}
