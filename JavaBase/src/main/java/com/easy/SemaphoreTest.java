package com.easy;

import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName SemaphoreTest
 * @Description 信号量测试
 * @Author zheng
 * @Date 2022/6/16 19:56
 * @Version 1.0
 **/
public class SemaphoreTest {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(10);
        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(() -> {
                try {
                    semaphore.acquire();
                    TimeUnit.MILLISECONDS.sleep(1000);
                    System.out.println("线程执行了");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    semaphore.release();
                }
            });
            thread.start();
        }
    }

}
