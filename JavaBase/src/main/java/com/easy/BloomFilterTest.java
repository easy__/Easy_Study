package com.easy;

import com.carrotsearch.sizeof.RamUsageEstimator;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

/**
 * @ClassName BoomFilterTest
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/14 15:03
 * @Version 1.0
 **/
public class BloomFilterTest {
    public static void main(String[] args) {
        int size = 10000000;
        BloomFilter<String> bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charset.defaultCharset()), size, 0.0001);
        Map<String, String> map = new HashMap<>();
        HashSet<String> set = new HashSet<>();
        for (int i = 0; i < size; i++) {
            String uuid = UUID.randomUUID().toString();
            bloomFilter.put(uuid);
            map.put(uuid, uuid);
            set.add(uuid);
        }
        int errorCount = 0;
        for (int i = size; i < size * 2; i++) {
            String uuid = UUID.randomUUID().toString();
            if (bloomFilter.mightContain(uuid)) {
                System.out.println(i);
                errorCount++;
            }
        }
        float rate = (float) errorCount / size;
        System.out.println("误判个数：" + errorCount);
        System.out.println("误判率：" + rate);
        System.out.println("内存占用大小：" + RamUsageEstimator.humanSizeOf(bloomFilter));
        /*Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i <size; i++) {
            map.put(i,i);
        }*/
        System.out.println("内存占用大小：" + RamUsageEstimator.humanSizeOf(map));
        System.out.println("内存占用大小：" + RamUsageEstimator.humanSizeOf(set));
    }
}
