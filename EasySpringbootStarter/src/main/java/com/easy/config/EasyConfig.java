package com.easy.config;

import com.easy.service.TestService;
import com.easy.service.impl.TestServiceImpl;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EasyConfig {
    @Bean
    public TestService getTestService(){
        return new TestServiceImpl();
    }

    @Bean
    public UserConfig userConfig(){
        return  new UserConfig();
    }
}
