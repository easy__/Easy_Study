package com.easy.service;

import java.util.List;

public interface TestService {
    public List<String> splitTest(String msg);
}
