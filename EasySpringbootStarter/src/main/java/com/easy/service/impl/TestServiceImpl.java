package com.easy.service.impl;

import com.easy.service.TestService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TestServiceImpl implements TestService {

    @Override
    public List<String> splitTest(String msg) {
        return Arrays.stream(msg.split(",")).collect(Collectors.toList());
    }
}
