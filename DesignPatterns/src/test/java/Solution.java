import java.util.*;


public class Solution {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * @param param string字符串
     * @return string字符串
     */
    public static String compressString(String param) {
        if (param==null||param.length()==0){
            return "";
        }
        StringBuffer sb = new StringBuffer();
        char[] chars = param.toCharArray();
        char tmp = chars[0];
        int count = 1;
        for (int i = 1; i < chars.length; i++) {
            if (chars[i] == tmp) {
                count++;
            } else {
                sb.append(tmp);
                if (count > 1) {
                    sb.append(count);
                }
                count = 1;
                tmp = chars[i];
            }
        }
        sb.append(tmp);
        if (count > 1) {
            sb.append(count);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(compressString("aabcccccaaa"));
    }
}