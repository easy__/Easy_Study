package com.easy.strategy;

public interface Discount {

    public float getPrice(float price);
}
