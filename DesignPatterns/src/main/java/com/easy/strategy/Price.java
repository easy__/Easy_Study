package com.easy.strategy;

/**
 * @ClassName Price
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/18 17:51
 * @Version 1.0
 **/
public class Price {
    private Discount discount;

    public Price(Discount discount) {
        this.discount = discount;
    }

    public float getPrice(float price) {
        return discount.getPrice(price);
    }
}
