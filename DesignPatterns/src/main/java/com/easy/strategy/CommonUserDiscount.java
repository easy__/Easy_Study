package com.easy.strategy;

/**
 * @ClassName CommonUserDiscount
 * @Description 普通用户的折扣
 * @Author zheng
 * @Date 2022/3/18 17:45
 * @Version 1.0
 **/
public class CommonUserDiscount implements Discount {
    @Override
    public float getPrice(float price) {
        return price * 0.85f;
    }
}
