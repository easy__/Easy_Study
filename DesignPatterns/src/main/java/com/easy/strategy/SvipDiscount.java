package com.easy.strategy;

/**
 * @ClassName SvipDiscount
 * @Description 超级vip打折策略
 * @Author zheng
 * @Date 2022/3/18 17:49
 * @Version 1.0
 **/
public class SvipDiscount implements Discount {
    @Override
    public float getPrice(float price) {
        price = price > 20 ? price - 20 : price;
        return price * 0.85f;
    }
}
