package com.easy.strategy;

/**
 * @ClassName VipDiscount
 * @Description vip 用户的打折策略
 * @Author zheng
 * @Date 2022/3/18 17:47
 * @Version 1.0
 **/
public class VipDiscount implements Discount {
    @Override
    public float getPrice(float price) {
        if (price > 10) {
            price -= 10;
        }
        return price * 0.85f;
    }
}
