package com.easy.strategy;

/**
 * @ClassName Strategy
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/18 17:53
 * @Version 1.0
 **/
public class StrategyTest {
    public static void main(String[] args) {
        float goodsPrice = 100;
        Price commonPrice = new Price(new CommonUserDiscount());
        System.out.println("普通会员的购买价格是" + commonPrice.getPrice(goodsPrice));
        Price vipPrice = new Price(new VipDiscount());
        System.out.println("vip的购买价格是" + vipPrice.getPrice(goodsPrice));
        Price sVipPrice = new Price(new SvipDiscount());
        System.out.println("sVip的购买价格是" + sVipPrice.getPrice(goodsPrice));
    }
}
