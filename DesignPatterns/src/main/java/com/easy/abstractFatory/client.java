package com.easy.abstractFatory;

import com.easy.abstractFatory.facotry.AbstractFactory;
import com.easy.abstractFatory.facotry.AliFactory;
import com.easy.abstractFatory.facotry.JxyFactory;
import org.checkerframework.checker.units.qual.A;

/**
 * @ClassName client
 * @Description TODO
 * @Author zheng
 * @Date 2022/8/13 16:37
 * @Version 1.0
 **/
public class client {
    public static void main(String[] args) {
        //AbstractFactory abstractFactory = new AliFactory();
        AbstractFactory abstractFactory = new JxyFactory();
        abstractFactory.asr(null);
        abstractFactory.tts("");
    }
}
