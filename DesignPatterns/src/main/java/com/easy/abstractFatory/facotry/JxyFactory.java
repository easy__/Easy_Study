package com.easy.abstractFatory.facotry;

import com.easy.abstractFatory.service.Asr;
import com.easy.abstractFatory.service.Tts;
import com.easy.abstractFatory.service.impl.JxyAsr;
import com.easy.abstractFatory.service.impl.JxyTts;

import java.io.File;

/**
 * @ClassName AliFactory
 * @Description TODO
 * @Author zheng
 * @Date 2022/8/13 16:21
 * @Version 1.0
 **/
public class JxyFactory implements AbstractFactory {


    @Override
    public String asr(File file) {
        return new JxyAsr().asr(file);
    }

    @Override
    public byte[] tts(String text) {
        return new JxyTts().tts(text);
    }
}
