package com.easy.abstractFatory.facotry;

import com.easy.abstractFatory.service.Asr;
import com.easy.abstractFatory.service.Tts;
import com.easy.abstractFatory.service.impl.AliAsr;
import com.easy.abstractFatory.service.impl.AliTts;

import java.io.File;

/**
 * @ClassName AliFactory
 * @Description TODO
 * @Author zheng
 * @Date 2022/8/13 16:21
 * @Version 1.0
 **/
public class AliFactory implements AbstractFactory {

    @Override
    public String asr(File file) {
        return new AliAsr().asr(file);
    }

    @Override
    public byte[] tts(String text) {
        return new AliTts().tts(text);
    }
}
