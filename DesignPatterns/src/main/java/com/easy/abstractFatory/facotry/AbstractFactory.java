package com.easy.abstractFatory.facotry;

import com.easy.abstractFatory.service.Asr;
import com.easy.abstractFatory.service.Tts;

import java.io.File;

/**
 * @Description 抽象工厂，对工厂的抽象，在本例子中是对不同公司的抽象
 * @Date 2022/8/13 16:26
 * @Param
 * @return
 **/
public interface AbstractFactory {

    public String asr(File file);

    public byte[] tts(String text);
}
