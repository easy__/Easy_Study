package com.easy.abstractFatory.service.impl;

import com.easy.abstractFatory.service.Tts;

/**
 * @ClassName AliTts
 * @Description TODO
 * @Author zheng
 * @Date 2022/8/13 16:09
 * @Version 1.0
 **/
public class JxyTts implements Tts {
    @Override
    public byte[] tts(String text) {
        System.out.println("极限元语音合成...");
        return new byte[0];
    }
}
