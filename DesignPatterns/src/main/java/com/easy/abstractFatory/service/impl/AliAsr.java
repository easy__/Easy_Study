package com.easy.abstractFatory.service.impl;

import com.easy.abstractFatory.service.Asr;

import java.io.File;

/**
 * @ClassName AliAsr
 * @Description 阿里云语音识别接口
 * @Author zheng
 * @Date 2022/8/13 11:30
 * @Version 1.0
 **/
public class AliAsr implements Asr {
    @Override
    public String asr(File file) {
        System.out.println("阿里语音识别接口调用。。。");
        return null;
    }
}
