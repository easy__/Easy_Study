package com.easy.abstractFatory.service;

/**
 * @ClassName Tts
 * @Description TODO
 * @Author zheng
 * @Date 2022/8/13 16:09
 * @Version 1.0
 **/
public interface Tts {

    public byte[] tts(String text);
}
