package com.easy.factory;

import com.easy.factory.asr.factory.AliAsrFactory;
import com.easy.factory.asr.factory.AsrFactory;
import com.easy.factory.asr.factory.JxyAsrFactory;
import com.easy.factory.asr.service.Asr;
import org.checkerframework.checker.units.qual.A;

/**
 * @ClassName Client
 * @Description 测试客户端
 * @Author zheng
 * @Date 2022/8/13 11:32
 * @Version 1.0
 **/
public class Client {
    public static void main(String[] args) {
        //AsrFactory asrFactory = new AliAsrFactory();
        AsrFactory asrFactory = new JxyAsrFactory();
        Asr asr = asrFactory.getAsr();
        String res = asr.asr(null);
    }
}
