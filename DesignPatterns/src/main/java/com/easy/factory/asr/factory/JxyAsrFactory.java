package com.easy.factory.asr.factory;

import com.easy.factory.asr.service.Asr;
import com.easy.factory.asr.service.impl.AliAsr;
import com.easy.factory.asr.service.impl.JxyAsr;

/**
 * @ClassName AliAsrFactory
 * @Description TODO
 * @Author zheng
 * @Date 2022/8/13 16:01
 * @Version 1.0
 **/
public class JxyAsrFactory implements AsrFactory{
    @Override
    public Asr getAsr() {
        return new JxyAsr();
    }
}
