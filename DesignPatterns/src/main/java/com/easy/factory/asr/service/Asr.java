package com.easy.factory.asr.service;

import java.io.File;

/**
 * @ClassName Asr
 * @Description 语音识别
 * @Author zheng
 * @Date 2022/8/13 11:31
 * @Version 1.0
 **/
public interface Asr {
    /**
     * @return java.lang.String
     * @Description 语音识别接口
     * @Date 2022/8/13 11:32
     * @Param [file]
     **/
    public String asr(File file);
}
