package com.easy.factory.asr.factory;

import com.easy.factory.asr.service.Asr;
import com.easy.factory.asr.service.impl.AliAsr;
import com.easy.factory.asr.service.impl.JxyAsr;

import java.io.File;

/**
 * @ClassName AsrFactory
 * @Description 语音识别工厂类
 * @Author zheng
 * @Date 2022/8/13 11:36
 * @Version 1.0
 **/
public interface AsrFactory {
    public Asr getAsr();
}
