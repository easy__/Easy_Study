package com.easy.singleton;

/**
 * @ClassName Singleton
 * @Description 单例模式
 * @Author zheng
 * @Date 2022/3/18 11:57
 * @Version 1.0
 **/
public class Singleton {
    private static final Singleton INSTANCE = new Singleton();

    private Singleton() {
    }

    public static Singleton getInstance() {
        return INSTANCE;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                System.out.println(Singleton.getInstance().hashCode());
            }).start();
        }
        //System.out.println(Singleton.getInstance().hashCode());
    }

}
