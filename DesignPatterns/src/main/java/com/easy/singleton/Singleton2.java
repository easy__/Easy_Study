package com.easy.singleton;

public enum Singleton2 {
    INSTANCE;

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                System.out.println(Singleton2.INSTANCE.hashCode());
            }).start();
        }

    }
}
