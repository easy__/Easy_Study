package com.easy.singleton;

/**
 * @ClassName Singleton1
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/18 14:43
 * @Version 1.0
 **/
public class Singleton1 {
    private Singleton1() {

    }

    /**
     * 静态内部类
     */
    private static class Singleton1Holder {
        private static final Singleton1 INSTANCE = new Singleton1();
    }

    public static Singleton1 getInstance() {
        return Singleton1Holder.INSTANCE;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                System.out.println(Singleton1.getInstance().hashCode());
            }).start();
        }
    }
}
