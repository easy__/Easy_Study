package com.easy.service;

/**
 * @ClassName PayService
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/29 14:56
 * @Version 1.0
 **/
public interface PayService {
    public String payMethod();
}
