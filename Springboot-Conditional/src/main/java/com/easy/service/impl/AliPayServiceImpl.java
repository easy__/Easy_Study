package com.easy.service.impl;

import com.easy.service.PayService;

/**
 * @ClassName AliPayService
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/29 14:57
 * @Version 1.0
 **/
public class AliPayServiceImpl implements PayService {
    @Override
    public String payMethod() {
        return "alipay";
    }
}
