package com.easy;

import com.easy.domain.ClassA;
import com.easy.domain.ClassB;
import com.easy.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName SpringbootApplication
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/29 14:51
 * @Version 1.0
 **/
@SpringBootApplication
@RestController
@EnableAspectJAutoProxy
public class SpringbootApplication {

    private final PayService payService;
    @Autowired
    private ClassA classA;
    @Autowired
    private ClassB classB;

    public SpringbootApplication(PayService payService) {
        this.payService = payService;
    }

    public static void main(String[] args) {

        SpringApplication.run(SpringbootApplication.class);
    }

    @GetMapping("/getPayMethod")
    public String pay() {
        classA.test();
        System.out.println(payService.payMethod());
        return payService.payMethod();
    }
}
