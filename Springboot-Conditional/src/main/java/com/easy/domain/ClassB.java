package com.easy.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName ClassB
 * @Description TODO
 * @Author zheng
 * @Date 2023/5/4 19:35
 * @Version 1.0
 **/
@Component
public class ClassB {
    @Autowired
    private ClassA classA;
}
