package com.easy.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName ClassA
 * @Description TODO
 * @Author zheng
 * @Date 2023/5/4 19:35
 * @Version 1.0
 **/
@Component
public class ClassA {
    @Autowired
    private ClassB classB;

    public void  test(){
        System.out.println("test class a");
    }
}
