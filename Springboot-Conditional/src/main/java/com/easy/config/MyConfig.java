package com.easy.config;

import com.easy.service.PayService;
import com.easy.service.impl.AliPayServiceImpl;
import com.easy.service.impl.WechatPayServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName MyConfig
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/29 15:05
 * @Version 1.0
 **/
@Configuration
public class MyConfig {
    @Bean
    @ConditionalOnProperty(name = "pay.method", havingValue = "alipay")
    public PayService aliPay() {
        return new AliPayServiceImpl();
    }

    @Bean
    @ConditionalOnProperty(name = "pay.method", havingValue = "wechatPay")
    public PayService wechatPay() {
        return new WechatPayServiceImpl();
    }
}
