package com.easy.config;

import com.easy.annotation.EnableAutoImportSelector;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName MyConfig
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/28 16:51
 * @Version 1.0
 **/
@Configuration
@ComponentScan("com.easy.component")
@EnableAutoImportSelector
public class MyConfig {
}
