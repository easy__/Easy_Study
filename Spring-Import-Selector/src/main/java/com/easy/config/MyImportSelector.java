package com.easy.config;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @ClassName MyImportSelector
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/28 17:45
 * @Version 1.0
 **/
public class MyImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{"com.easy.component.Horse"};
    }
}
