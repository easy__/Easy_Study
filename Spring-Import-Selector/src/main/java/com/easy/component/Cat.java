package com.easy.component;

/**
 * @ClassName Cat
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/28 17:06
 * @Version 1.0
 **/
public class Cat {

    @Override
    public String toString() {
        String name = "cat";
        return "Cat{" +
                "name='" + name + '\'' +
                '}';
    }
}
