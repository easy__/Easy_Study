package com.easy.component;

import org.springframework.stereotype.Component;

/**
 * @ClassName Dog
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/28 16:47
 * @Version 1.0
 **/
@Component
public class Dog {

    @Override
    public String toString() {
        String name = "dog";
        return "Dog{" +
                "name='" + name + '\'' +
                '}';
    }
}
