package com.easy.component;

/**
 * @ClassName Horse
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/28 17:44
 * @Version 1.0
 **/
public class Horse {

    @Override
    public String toString() {
        String name = "horse";
        return "Horse{" +
                "name='" + name + '\'' +
                '}';
    }
}
