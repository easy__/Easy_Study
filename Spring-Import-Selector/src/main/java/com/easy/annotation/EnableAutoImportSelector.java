package com.easy.annotation;

import com.easy.component.Cat;
import com.easy.config.MyImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @ClassName EnableAutoImportSelector
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/28 17:48
 * @Version 1.0
 **/
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target(ElementType.TYPE)
@Import(value = {Cat.class, MyImportSelector.class})
public @interface EnableAutoImportSelector {
}
