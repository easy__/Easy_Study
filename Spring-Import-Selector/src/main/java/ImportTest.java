import com.easy.component.Cat;
import com.easy.component.Dog;
import com.easy.component.Horse;
import com.easy.config.MyConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @ClassName ImportTest
 * @Description TODO
 * @Author zheng
 * @Date 2022/3/28 16:49
 * @Version 1.0
 **/
public class ImportTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(MyConfig.class);
        //Component 注解获取bean
        Dog dog = annotationConfigApplicationContext.getBean(Dog.class);
        System.out.println(dog.toString());
        //@Import导入相关类
        Cat cat = annotationConfigApplicationContext.getBean(Cat.class);
        System.out.println(cat.toString());
        //ImportSector
        Horse horse = annotationConfigApplicationContext.getBean(Horse.class);
        System.out.println(horse.toString());

    }
}
